Un exemple d'algorithme génétique utilisé pour trouver une solution permettant de faire avancer un bras robotisé.
Necessite le simulateur Marilou (http://anykode.com/index.php) pour compiler / fonctionner.

A noter, bien qu'il devrait fonctionner normalement sous Linux, en pratique le simulateur se bloquait de temps en temps. A utiliser sous Windows donc.


Script utile :
compile.sh, pour les options de compilations souvent utilisée et longues à taper. Si vous n'en voulez pas, bah utiliser cmake normalement.