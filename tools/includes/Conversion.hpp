#pragma once

#include <string>
#include <sstream>

/*!
 * @namespace Conversion
 * @brief Les fonctions de conversion
 */
namespace Conversion
{
  /*!
   * @fn T       FromString (const std::string &str)
   * \brief Convert a std::string into T using a std::stringstream
   * @param str La std::string qui sera convertie
   * @return La valeur convertie
   * @todo Cas particuler a unifier, bool
   */
  template<typename T>
  T       FromString(const std::string &str)
  {
    std::stringstream     ss(str);
    T                     value;

    ss >> value;
    return (value);
  }

  /*!
   * @fn std::string     ToString (const T &value)
   * @brief Convert T into a std::string using a std::stringstream
   * @param value La valeur a convertir en std::string
   * @return Une std::string contenant la valeur convertie
   */
  template<typename T>
  std::string     ToString(const T &value)
  {
    std::stringstream     ss;

    ss << value;
    return (ss.str());
  }

  /*!
   * \brief Conversion une chaîne de characteres en booleen. Non sensible a la casse.
   * \attention Renvoie true dans tous les cas qui ne sont pas false.
   * \param str La chaîne de characteres a convertir.
   * \return false si str == "0" || str == "false", true sinon
   */
  bool  ToBool(std::string str);

  /*!
   * @fn void IntToBase(unsigned int value, const std::string &base, std::string &buffer)
   * \brief modifie buffer en fonction de value & de base
   * @param value Une valeur non signee a convertir (Sur 32 bits)
   * @param base La base dans laquelle value sera convertie
   * @param buffer Une reference sur une string qui contiendra le resultat
   */

  void    IntToBase(unsigned int value, const std::string &base, std::string &buffer);

  /*!
   * \fn std::string IntToBase(unsigned int value, const std::string &base)
   * \brief Retourne value sous forme d'une std::string en fonction de la base base
   * @param value Une valeur non signee a convertir (Sur 32 bits)
   * @param base La base dans laquelle value sera convertie
   * @return La valeur convertie selon base, sous forme d'une std::string
   */
  std::string IntToBase(unsigned int value, const std::string &base);
}

