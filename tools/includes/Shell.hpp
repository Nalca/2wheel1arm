#pragma once

#include <iostream>
#include <string>
#include <list>
#include <map>
#include "Exception.hpp"

/**
 * \class Shell
 * \brief Un minishell a integrer dans un programme pour avoir une interface basique
 * \todo Un moyen de relancer le shell proprement.
 * \todo A tester sur windows
 * \todo limite de instanceCounter a mettre de façon modifiable
 * \todo Descendre l'exception d'un niveau de namespace (Shell::ShellException -> ShellException)
 */
class Shell
{
public:
  typedef void (*funcPtr)(void *data, const std::list<std::string> &listArg);
  typedef void (Shell::*cFuncPtr)(const std::list<std::string> &listArg);

  /**
   * \struct Funcptr_externe
   * \brief La structure contenant le pointeur, la donnees associees au pointeur (si presente).
   * \attention La structure est detruite par le shell a lors de sa destruction.
   * \param funcptr Un pointeur de type funcPtr.
   * \param data Un pointeur qui sera utilisee lors de l'appel de la fonction.
   * \param nbArgument Le nombre d'argument de la commande. -1 si pas de limite.
   * \param name Le nom de la commande.
   * \param description Une petite phrase decrivant l'action de la commande.
   * \param usage Comment utiliser la fonction (Description des parametres).
   */
  struct Funcptr_externe
  {
    funcPtr        funcptr;
    void           *data;
    std::string    name;
    std::string    description;
    std::string    usage;

    /// \brief Une methode statique permettant d'obtenir un objet valide et deja rempli.
    static Funcptr_externe *get(funcPtr fptr, void *data, const char *_name, const char *_desc, const char *_help);
  };

  /**
   * \struct Funcptr_builtin
   * \brief Une structure utilisee en interne pour les builtins.
   * \internal Fonctionne a peu pres comme <b>Funcptr_externe</b>
   * \param funcptr Un pointeur de type cFuncPtr.
   * \param nbArgument Le nombre d'argument de la commande. -1 si pas de limite.
   * \param name Le nom de la commande.
   * \param description Une petite phrase decrivant l'action de la commande.
   * \param usage Comment utiliser la fonction (Description des parametres).
   */
  struct Funcptr_builtin
  {
    cFuncPtr       funcptr;
    std::string    name;
    std::string    description;
    std::string    usage;

    /// \brief Une methode statique permettant d'obtenir un objet valide et deja rempli.
    static Funcptr_builtin *get(cFuncPtr fptr, const char *_name, const char *_desc, const char *_help);
  };

  /**
   * \class Shell::ShellException
   * \brief Exceptions liees au shell.
   */
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  NEW_EXCEPTION(ShellException, BasicException);
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

public:
  /// @brief Constructeur par default du shell.
  /// @param _prompt Le prompt qui sera affiche pour indiquer l'attente d'une nouvelle commande.
  Shell(const char *_prompt = "$:");
  /// @brief Destructeur du shell.<br>Delete les pointeurs contenus dans <b>funcPtr_in</b> et <b>funcPtr_ex</b>
  virtual ~Shell();

  /**
  * @fn Shell(const char *_prompt);
  * @brief Constructeur par default du shell.
  * @param _prompt Le prompt qui sera affiche pour indiquer l'attente d'une nouvelle commande.
  *
  * @fn void    Run(std::istream &is = std::cin);
  * @brief Lance le shell. Ne rend la main que quand exit est appele.<br>Ne catch pas d'exception.<br>Peut lire depuis d'autres sources que std::cin
  *
  * @fn void    AddFuncPtr(Funcptr_externe *funcptr);
  * @brief Permet d'ajouter une commande au shell.
  * @throw Shell::ShellException si funcptr == nullptr
  * @todo insulter l'utilisateur si la fonction existe deja
  *
  * @fn const std::string &GetEnvVar(const std::string &varname) const;
  * @brief Retourne la valeur de <b>varname</b> sous forme de reference constante.
  * @throw Shell::ShellException si varname n'est pas definie.
  */
  void    Run(std::istream &is = std::cin, bool showPrompt = true);
  void    AddFuncPtr(Funcptr_externe *funcptr);
  void    setenv(std::string varname, const std::string &value);

  /// \brief Renvoie la taille de l'environnement du shell.
  std::size_t GetEnvSize() const;
  const std::string &GetEnvVar(std::string varname) const;

  /// \brief Un booleen qui permet de quitter la boucle dans Shell::Run.
  bool                                        isRunning;

  /**
   * \fn void    readStream(std::istream &is, std::string &command, std::list<std::string> &listArg);
   * \brief Lit une ligne du stream <b>is</br>, met le premier mot dans <b>command</b> et le reste dans <b>listArg</b>
   * \param is Un istream qui sera lu. Accepte les fichiers.
   * \fn void    inputError(const std::string &str);
   * \brief Affiche un message d'erreur quand la commande n'est pas reconnue.
   */
private:
  void    readStream(std::istream &is, std::string &command, std::list<std::string> &listArg);
  void    inputError(const std::string &str);

  /**
  * \fn void    exit(const std::list<std::string> &listArg);
  * \brief Termine le shell.
  * \fn void    help(const std::list<std::string> &listArg);
  * \brief Affiche l'aide et les commandes disponibles.
  */
  void    exit(const std::list<std::string> &listArg);
  void    help(const std::list<std::string> &listArg);

  /**
   * \fn void    readconf(const std::list<std::string> &listArg);
   * \brief Lit un ou plusieurs fichiers contenant des commandes du shell. (Un script quoi).
   * \fn void    setenv(const std::list<std::string> &listArg);
   * \brief Set une variable d'environnement. <b>Varname Value</b>
   */
  void    readconf(const std::list<std::string> &listArg);
  void    setenv(const std::list<std::string> &listArg);

  /// \brief Supprime une variable d'environnement.
  void    unsetenv(const std::list<std::string> &listArg);

  /// \brief Affiche les variables d'environnement.
  void    env(const std::list<std::string> &listArg);

  /// \brief Fais un certain nombre de \n.
  void    clear(const std::list<std::string> &listArg);

private:
  /**
   * \var instanceCounter
   * \brief Un compteur de nombre d'appel de Shell::Run, pour eviter les boucles infinies dans les scripts.
   * \var prompt
   * \brief Le prompt affiche avant chaque commande, qui indique l'attente d'une commande.
   * \var funcPtr_in
   * \brief Une map contenant les pointeurs sur fonctions internes.
   * \var funcPtr_ex
   * \brief Une map contenant les pointeurs sur fonctions externes.
   * \var envMap
   * \brief Une map contenant les variables d'environnement.
   */
  std::size_t                                 instanceCounter;
  std::string                                 prompt;
  std::map<std::string, Funcptr_builtin *>    funcPtr_in;
  std::map<std::string, Funcptr_externe *>    funcPtr_ex;
  std::map<std::string, std::string>          envMap;
};
