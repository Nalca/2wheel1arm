#pragma once

/*!
 * \def __func__
 * \brief Donne le nom de la fonction appelant cette macro
 * \todo L'implementation pour Windows. (A tester sur Windows)
 */

#ifndef __GNUG__
#define __func__ __FUNCTION__
#endif

 /// \todo Une macro a mettre dans les parametres d'une fonction pour pas qu'elle gueule quand ses arguments sont inutilises


#define DELETE_VECTOR(type, vectorobj) for (std::vector<type>::iterator it = vectorobj.begin(); it != vectorobj.end(); it++) \
          delete *it; \
        vectorobj.clear()

#ifdef __linux__
#define OS_LINUX
#elif _WIN32
#define OS_WINDOWS
#endif