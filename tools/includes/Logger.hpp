#pragma once

#include <string>
#include <iostream>
#include <map>
#include "Color.hpp"

/*!
 * \class Logger
 * \brief Un loggeur de message pour faire du log simplement.
 * \attention Le constructeur/destructeur ne doit <b>jamais</b> etre appele par l'utilisateur.
 */
class Logger
{
private:
  class LoggerStream;

public:
  /// \typedef LogLevel
  /// \brief Une valeur de type <b>unsigned int</b>
  typedef unsigned int LogLevel;
  /*!
   * \var Message
   * \brief Affiche <b>[Message]</b> en vert avant d'imprimer le message sur std::cout
   * \var Warning
   * \brief Affiche <b>[Warning]</b> en orange avant d'imprimer le message sur std::cout
   * \var Error
   * \brief Arriche <b>[Erreur]</b> en rouge avant d'imprimer le message sur std::cerr
   */
  static const LogLevel Message;
  static const LogLevel Warning;
  static const LogLevel Error;

  /// \fn   static void   message(LogLevel level, const std::string &msg)
  /// \brief Ecrit un message selon son niveau de log
  static void   message(LogLevel level, const std::string &msg);

  /// \fn static void   message(LogLevel level, const char *msg)
  /// \brief Ecrit un message selon son niveau de log
  static void   message(LogLevel lvl, const char *msg);

  /**
   * \fn static void   setPrefix(LogLevel level, const std::string &Prefix, Color::Color colorPrefix, Color::Color colorMsg)
   * \brief Permet de mettre un prefix (+ Couleur) qui apparaîtra toujours avant un message
   * \param level Le niveau de log du prefix
   * \param Prefix Une std::string a afficher avant chaque message
   * \param colorPrefix La couleur du prefix
   * \param colorMsg La couleur du message
   */
  static void   setPrefix(LogLevel level,
                          const std::string &Prefix,
                          Color::Color colorPrefix,
                          Color::Color colorMsg);
  /**
   * \fn static void   open(LogLevel level, std::ostream *streamptr, bool DeleteAtClose = false);
   * \brief Initialise le logger en donnant des references sur les std::ostream utilises + les trois de bases
   * \param level Un LogLevel inutilise (Abort si deja utilise)
   * \param streamptr Un pointeur sur un std::ostream
   * \param DeleteAtClose Indique si le std::ostream doit etre delete en meme temps que le LoggerStream
   */
  static void   open(LogLevel level, std::ostream *streamptr, bool DeleteAtClose = false);

  /// \fn static void   open();
  /// \brief Initialise le logger avec les trois stream par default (Message, Warning, Error)
  static void   open();

  /// \fn static void   close()
  /// \brief Delete les LoggerStream
  static void   close();

  /// \fn static void   close(LogLevel level);
  /// \brief Delete le log associe au LogLevel
  /// \param level le level de log (aka, le level associe a un ostream)
  static void   close(LogLevel level);

private:
  static Logger *logPtr;
  std::map<LogLevel, LoggerStream *>  OstreamMap;

  Logger();
  ~Logger();

  /// Unused
  Logger(const Logger &other);


private:
  /// @class LoggerStream
  /// @brief Une classe qui contient le ostream + prefix + couleur et delete le ostream (si demande)
  class LoggerStream
  {
  public:
    LoggerStream(std::ostream *_os,
                 const std::string &_prefixTxt,
                 Color::Color _prefixColor,
                 Color::Color _textColor,
                 bool _DeleteAtClose
                );
    virtual ~LoggerStream();

    std::ostream    *outputStream;
    std::string     prefixText;
    Color::Color    prefixColor;
    Color::Color    textColor;
    bool            DeleteAtClose;

    void  message(const char *str);
    void  setAttribut(const std::string &_prefixText, Color::Color _prefixColor, Color::Color _textColor);

  private:
    LoggerStream();
  };
};
