#pragma once

#include <iostream>

/*!
 * \namespace Color
 * \brief Permet d'afficher en couleur dans un vrai terminal.
 */
namespace Color
{
  /*!
   * \enum Color
   * \brief La liste des couleurs possibles
   */
  enum Color
  {
    white,
    red,
    green,
    yellow,
    blue,
    violet,
    origine
  };

  /*!
   * \fn void useColor(bool state)
   * \brief Permet l'utilisation ou non des couleurs
   * \param state Si false, n'affiche pas les couleurs
   */
  void  useColor(bool state);
}

/*!
 * \fn std::ostream    &operator<< (std::ostream &os, const Color::Color &color);
 * \brief Utilisation : std::cout << Color::red
 */
std::ostream    &operator<< (std::ostream &os, const Color::Color &color);
