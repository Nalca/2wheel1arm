#pragma once

#include <string>
#include <list>
#include <map>
#include "Exception.hpp"

/**
 * \namespace Options
 * \brief Le namespace contenant les class reliees a la gestion des arguments d'entree du programme.
 * \todo Gerer l'option 'jusqu'a la prochaine option'
 * \todo Changer le nom du gestionnaire d'IOptionnable, car Options::Options c'est ULTRA daube comme nom
 */
namespace Options
{
  /**
   * \class Options::OptionsException
   * \brief Exceptions liees a la gestion d'argument.
   */
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  NEW_EXCEPTION(OptionsException, BasicException);
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  class IOptionnable;
  typedef std::list<char *>                           Arguments;
  typedef std::map<const std::string, IOptionnable *> MapOptions;

  /**
   * \class Options::IOptionnable
   * \brief Une interface necessaire pour implementer un argument.
   */
  class IOptionnable
  {
  public:
    /**
     * \fn explicit IOptionnable (int NbArg, const std::string &Msg)
     * \brief Le contructeur qui <b>doit</b> etre utilise.
     * \param NbArg Le nombre d'argument attendu (obligatoirement).
     * \param Msg Le message d'aide a afficher.
     */
    explicit IOptionnable(int NbArg, const std::string &Msg) : NbArg(NbArg), HelpMsg(Msg) {};
    virtual   ~IOptionnable() {};
    /**
     * \fn virtual void Execute (Arguments &) = 0;
     * \brief La fonction qui sera appelee quand l'argument sera rencontre.
     * \param arg La liste d'argument sour forme de char *.
     */
    virtual   void Execute(Arguments &arg) = 0;

    const int         NbArg;
    const std::string HelpMsg;

  private:
    IOptionnable();
  };

  /**
   * \class Options::Options
   * \brief Le gestionnaire d'IOptionnable, gere les arguments d'entree du programme.
   */
  class Options
  {
  public:
    /**
     * \fn Options (const char *progName = "", bool stopAtFirstUnknowArg = true)
     * \brief Le constructeur par default du gestionnaire d'options.
     * \param progName Le nom du programme, qui sera affiche lors l'aide est affichee.
     * \param stopAtFirstUnknowArg Lancera une exception au premier argument inconnu.
     */
    Options(const char *progName = "", bool stopAtFirstUnknowArg = true);
    ~Options();

    /**
     * \fn void AddOptions (const std::string &ArgName, IOptionnable *)
     * \brief Ajoute une nouvelle option au gestionnaire.
     * \param ArgName La chaine a reconnaitre pour executer l'option.
     * \param opt Un pointeur sur un objet IOptionnable alloue (qui sera delete automatiquement).
     * \throw OptionsException Si deux ArgName identiques sont ajoutes.
     */
    void      AddOptions(const std::string &ArgName, IOptionnable *opt);

    /**
     * \fn void      ReadArg (int argc, char *argv[], Arguments &listString)
     * \brief Lit les arguments dans argv, execute les options rencontrees. <br>
     *        Selon les options, peut renvoyer toutes les options rencontrees apres l'argument stoppant <br>
     *        S'arreter au premier argument inconnu. <br>
     *        Et stocker les arguments inutilises dans listString.
     * \param argc argc de main.
     * \param argv argv de main.
     * \param listString Une reference sur une liste qui stockera les arguments inutilises.
     * \throw OptionsException En cas d'argument manquant/inconnu.
     */
    void      ReadArg(int argc, char *argv[], Arguments &listString);

    /**
     * \fn void      ReadArg (int argc, char *argv[])
     * \brief Lit les arguments dans argv, execute les options rencontrees. <br>
     *        Selon les options, peut renvoyer toutes les options rencontrees apres l'argument stoppant <br>
     *        S'arreter au premier argument inconnu.
     * \param argc argc de main.
     * \param argv argv de main.
     * \throw OptionsException En cas d'argument manquant/inconnu.
     */
    void      ReadArg(int argc, char *argv[]);

    /**
     * \fn void      SetStoppingArg (const std::string &str)
     * \brief Change l'argument qui arretera le parsing d'argument.
     * \param str La nouvelle std::string qui stoppera le parsing des arguments.
     */
    void      SetStoppingArg(const std::string &str);

  private:
    void              PrintHelp() const;

    MapOptions        UsedOptions;
    std::string       _progName;
    bool              _stopAtFirstUnknowArg;
    std::string       _stoppingArg;

  public:
    /**
     * \fn const T &GetObject () const
     * \brief Retrouve l'objet demande selon son type (IOptionnable obligatoire)
     * \return Une reference constante sur l'objet <b>T</b> si il est dans le gestionnaire
     * \throw OptionsException Si l'objet <b>T</b> n'est pas trouve
     */
    template<typename T>
    const T &GetObject() const
    {
      MapOptions::const_iterator   cit;
      const T    *resultConv = nullptr;

      for (cit = UsedOptions.begin(); cit != UsedOptions.end(); cit++)
        {
          resultConv = dynamic_cast<const T *>(cit->second);
          if (resultConv != nullptr)
            return (*resultConv);
        }
      throw OptionsException("Options demandee inconnue");
    };

  private:
    Options();
  };
}
