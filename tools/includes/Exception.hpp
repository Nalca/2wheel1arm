#pragma once

#include <exception>
#include <string>
#include "Macro.hpp"

/**
 * \def NEW_EXCEPTION(newName, origine)
 * \brief Declare une nouvelle exception newName heritant de origine.
 */

#define NEW_EXCEPTION(newName, origine)     \
  class newName : public origine                                                            \
  {                                                                                       \
  public:                                                                                 \
    explicit newName(const std::string &str) throw() : origine(str)    {};                \
    virtual ~newName() throw()                                     {};                    \
  }

/// \brief Une exception qui herite de std::exception et implemente what().
class BasicException : public std::exception
{
public:
  explicit BasicException(const std::string &str) throw() : msg(str)    {};
  virtual ~BasicException() throw()                                     {};

  /// \brief Retourne le message contenu dans l'exception.
  /// \return Le message contenu dans l'exception sous forme de char*.
  virtual const char *what() const throw()
  {
    return (msg.c_str());
  };

protected:
  const std::string     msg;
};
