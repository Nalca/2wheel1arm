#pragma once

#include <vector>

/*!
 * @class Pool
 * @brief Une pool d'element, qui peut eventuellement etre utilisee grâce aux operateurs new et delete
 * @brief Mode d'emploi :
 * Surcharger l'operator new et l'operator delete : <br>
 * Syntaxe de new/delete : <br>
 * void T::<b>operator delete</b>(void *ptr) <br>
 * void *T::<b>operator new</b>(std::size_t size) <br>
 * <br>
 * Les faires appeller deleteElement pour delete & newElement pour new.
 * Les constructeurs/destructeurs seront appeles automatiquement <br>
 * <br>
 * Autre utilisation : On peut ne pas surcharger les deux operateurs et utiliser
 * deleteElement & getElement pour utiliser une pool par thread.
 */
template <typename T>
class Pool
{
public:
  /*!
   * @fn Pool(std::size_t _StartingSize = 256, std::size_t nbAlloc = 500)
   * @brief Constructeur par default
   * @param _StartingSize Le nombre d'element lors de l'instanciation
   * @param nbAlloc Le nombre d'element qui seront alloues en meme temps a chaque fois qu'il y aura une allocation d'elements
   */
  Pool(std::size_t _StartingSize = 256, std::size_t nbAlloc = 500) : nbElementAlloc(nbAlloc), NombreElementAlloue(0)
  {
    resize(_StartingSize);
  };

  /// @fn virtual ~Pool()
  /// @brief Supprime tout les elements de la pile (mais pas les autres)
  virtual ~Pool()
  {
    for (typename std::vector<T *>::iterator it = PileElement.begin(); it != PileElement.end(); it++)
      _freeElement(*it);
    PileElement.clear();
  }

  /*!
   * \fn virtual T     *newElement()
   * \brief A appeller par la surchage de new, renvoie un element en l'allouant reellement si besoin
   * @return L'element qui vient d'etre pris de la pile
   */
  virtual T     *newElement()
  {
    T   *ptr;

    if (PileElement.empty())
      {
        for (std::size_t i = 0; i < nbElementAlloc; i++)
          PileElement.push_back(_allocElement());
        NombreElementAlloue += nbElementAlloc;
      }
    ptr = PileElement.back();
    PileElement.pop_back();
    return (ptr);
  }

  /*!
   * @fn virtual void  deleteElement(void *ptr)
   * @brief A appeller par la surchage de delete, replace l'element dans la pile et le considere comme etant delete
   * @param ptr Un pointeur sur un element a remettre dans la pile
   */
  virtual void  deleteElement(void *ptr)
  {
    PileElement.push_back(static_cast<T *>(ptr));
  }

  /*!
   * \fn virtual void  resize(std::size_t size)
   * \brief Change la taille de la pool. <br>
   * Supprime ceux dans la pile si ils sont disponibles et en trop, en rajoute si besoin.
   * @param size La nouvelle taille de la pile (Prend en compte les elements deja alloues)
   */
  virtual void  resize(std::size_t size)
  {
    std::size_t NbAlloc = 0;

    if (size > NombreElementAlloue)
      {
        std::size_t Capacity = PileElement.capacity();

        PileElement.reserve(size);
        for (NbAlloc = NombreElementAlloue; NbAlloc < Capacity; ++NbAlloc)
          PileElement.push_back(_allocElement());
        NombreElementAlloue += NbAlloc;
      }
    else
      if (size < NombreElementAlloue)
        {
          while (PileElement.size() > 0 && NbAlloc < size)
            {
              _freeElement(PileElement.back());
              PileElement.pop_back();
              ++NbAlloc;
            }
          NombreElementAlloue -= NbAlloc;
        }
  }

private:
  /*!
   * \fn T *_allocElement()
   * \brief Alloue <b>vraiment</b> un nouvel element
   * @return Le nouvel element qui vient d'etre alloue
   */
  T *_allocElement()
  {
    return (static_cast<T *>(::operator new(sizeof(T))));
  }

  /*!
   * \fn void _freeElement(T *ptr)
   * \brief Delete <b>vraiment</b> un element
   * @param ptr L'element qui sera vraiment delete
   */
  void _freeElement(T *ptr)
  {
    ::operator delete(ptr);
  }

  const std::size_t     nbElementAlloc;
  std::vector<T *>      PileElement;
  std::size_t           NombreElementAlloue;
};
