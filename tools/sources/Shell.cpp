#include <sstream>
#include <fstream>
#include <algorithm>
#include "Shell.hpp"
#include "Macro.hpp"

Shell::Shell(const char *_prompt) : isRunning(true), instanceCounter(0), prompt(_prompt)
{
  funcPtr_in["exit"] = Funcptr_builtin::get(&Shell::exit, "exit", "Termine le shell", "");
  funcPtr_in["help"] = Funcptr_builtin::get(&Shell::help, "help", "Affiche l'aide", "");
  funcPtr_in["readconf"] = Funcptr_builtin::get(&Shell::readconf, "readconf",
                           "Lit un fichier de conf pour l'environnement",
                           "[filename]");
  funcPtr_in["setenv"] = Funcptr_builtin::get(&Shell::setenv, "setenv",
                         "Set une variable d'environnement",
                         "[varname] [value]");
  funcPtr_in["unsetenv"] = Funcptr_builtin::get(&Shell::unsetenv, "unsetenv",
                           "Supprime une variable d'environnement",
                           "[varname]");
  funcPtr_in["env"] = Funcptr_builtin::get(&Shell::env, "env", "Affiche l'environnement", "");
  funcPtr_in["clear"] = Funcptr_builtin::get(&Shell::clear, "clear", "Nettoie le terminal", "");
}

Shell::~Shell()
{
  for (std::map<std::string, Funcptr_builtin *>::iterator it = funcPtr_in.begin(); it != funcPtr_in.end(); it++)
    delete(it->second);
  for (std::map<std::string, Funcptr_externe *>::iterator it = funcPtr_ex.begin(); it != funcPtr_ex.end(); it++)
    delete(it->second);
}

void Shell::Run(std::istream &is, bool showPrompt)
{
  std::string                                           command;
  std::list<std::string>                                listWord;
  std::map<std::string, Funcptr_builtin *>::iterator    cfpIt;
  std::map<std::string, Funcptr_externe *>::iterator    fpIt;

  isRunning = true;
  ++instanceCounter;
  if (instanceCounter >= 10)
    {
      std::cout << "Nombre de script lu trop important. Arret de la lecture." << std::endl;
      return;
    }
  while (this->isRunning && is.good())
    {
      command.clear();
      listWord.clear();
      if (showPrompt)
        std::cout << prompt << std::flush;
      readStream(is, command, listWord);

      // Execute builtin
      if (command.size() > 0)
        {
          if ((cfpIt = funcPtr_in.find(command)) != funcPtr_in.end())
            {
              (this->*(cfpIt->second->funcptr))(listWord);
            }
          // Execute extern
          else if ((fpIt = funcPtr_ex.find(command)) != funcPtr_ex.end())
            {
              (fpIt->second->funcptr)(fpIt->second->data, listWord);
            }
          // Affichage erreur
          else
            {
              this->inputError(command);
            }
        }
    }
  --instanceCounter;
}

void    Shell::AddFuncPtr(Funcptr_externe *funcptr)
{
  if (funcptr)
    {
      funcPtr_ex[funcptr->name] = funcptr;
    }
  else
    throw Shell::ShellException("Pointeur null dans " + std::string(__func__));
}

void    Shell::setenv(std::string varname, const std::string &value)
{
  std::transform(varname.begin(), varname.end(), varname.begin(), toupper);
  envMap[varname] = value;
}

std::size_t Shell::GetEnvSize() const
{
  return (this->envMap.size());
}

const std::string &Shell::GetEnvVar(std::string varname) const
{
  std::transform(varname.begin(), varname.end(), varname.begin(), toupper);
  std::map<std::string, std::string>::const_iterator cit = envMap.find(varname);

  if (cit == envMap.end())
    throw Shell::ShellException("Variable d'environnement " + varname + " inexistante");
  else
    return (cit->second);
}

// Private ------------------------------------------------------------------------
void    Shell::readStream(std::istream &is, std::string &command, std::list<std::string> &listArg)
{
  std::stringstream    ss;
  std::string                line;

  std::getline(is, line);
  ss.str(line);
  ss >> command;
  while (ss.good())
    {
      std::string str;

      ss >> str;
      listArg.push_back(str);
    }
}

void    Shell::inputError(const std::string &str)
{
  std::cout << "Commande invalide : " << str << std::endl;
}

void    Shell::exit(const std::list<std::string> &listArg)
{
  if (listArg.size() == 0)
    isRunning = false;
  else
    inputError("Nombre d'argument invalide.");
}

void    Shell::help(const std::list<std::string> &listArg)
{
  std::map<std::string, Funcptr_builtin *>::const_iterator bcit;
  std::map<std::string, Funcptr_externe *>::const_iterator    cit;

  (void)listArg;
  std::cout << "Liste des commandes internes" << std::endl;
  for (bcit = funcPtr_in.begin(); bcit != funcPtr_in.end(); bcit++)
    std::cout << "  " << bcit->second->name << " " << bcit->second->usage << " : " << bcit->second->description << std::endl;
  std::cout << std::endl << "Autre commande : " << std::endl;
  for (cit = funcPtr_ex.begin(); cit != funcPtr_ex.end(); cit++)
    std::cout << "  " << cit->second->name << " " << cit->second->usage << " : " << cit->second->description << std::endl;
  std::cout << std::endl;
}

void    Shell::readconf(const std::list<std::string> &listArg)
{
  if (listArg.size() > 0)
  {
    for (std::list<std::string>::const_iterator cit = listArg.begin(); cit != listArg.end(); cit++)
      {
        std::fstream    fs;

        fs.open((*cit));
        if (fs.good())
          this->Run(fs);
      }
  }
  else
    inputError("Nombre d'argument invalide.");
}

void    Shell::setenv(const std::list<std::string> &listArg)
{
  if (listArg.size() == 2)
  {
    std::string    varname = listArg.front();
    std::string    value = listArg.back();

    std::transform(varname.begin(), varname.end(), varname.begin(), toupper);
    envMap[varname] = value;
  }
  else
    inputError("Nombre d'argument invalide.");
}

void    Shell::unsetenv(const std::list<std::string> &listArg)
{
  if (listArg.size() == 1)
  {
    std::string varname = listArg.front();
    std::map<std::string, std::string>::iterator    it;

    std::transform(varname.begin(), varname.end(), varname.begin(), toupper);
    if ((it = envMap.find(varname)) != envMap.end())
      envMap.erase(it);
  }
  else
    inputError("Nombre d'argument invalide.");
}

void    Shell::env(const std::list<std::string> &listArg)
{
  if (listArg.size() == 0)
  {
    std::map<std::string, std::string>::const_iterator    cit;

    (void)listArg;
    for (cit = envMap.begin(); cit != envMap.end(); cit++)
      std::cout << cit->first << " = " << cit->second << std::endl;
  }
  else
    inputError("Nombre d'argument invalide.");
}

void    Shell::clear(const std::list<std::string> &listArg)
{
  (void)listArg;
  for (std::size_t i = 0; i < 48; i++)
    std::cout << std::endl;

#if defined _WIN32
  system("cls");
#elif defined __linux__
  system("clear");
#endif
}

// Struct -----------------------------------------------------------------------------
Shell::Funcptr_externe *Shell::Funcptr_externe::get(funcPtr fptr, void *data, const char *_name, const char *_desc, const char *_help)
{
  Funcptr_externe *ptr = new Funcptr_externe;
  ptr->funcptr = fptr;
  ptr->data = data;
  ptr->name = _name;
  ptr->description = _desc;
  ptr->usage = _help;

  return (ptr);
}

Shell::Funcptr_builtin *Shell::Funcptr_builtin::get(cFuncPtr fptr, const char *_name, const char *_desc, const char *_help)
{
  Funcptr_builtin *ptr = new Funcptr_builtin;
  ptr->funcptr = fptr;
  ptr->name = _name;
  ptr->description = _desc;
  ptr->usage = _help;

  return (ptr);
}
