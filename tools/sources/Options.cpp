#include <cstdlib>
#include <iostream>
#include "Options.hpp"

namespace Options
{
  Options::Options(const char *progName, bool stopAtFirstUnknowArg)
    : _progName(progName), _stopAtFirstUnknowArg(stopAtFirstUnknowArg),
      _stoppingArg("")
  {}

  Options::~Options()
  {
    MapOptions::iterator  it;

    for (it = UsedOptions.begin(); it != UsedOptions.end(); it++)
      {
        delete(it->second);
      }
    UsedOptions.clear();
  }

  void   Options::AddOptions(const std::string &ArgName, IOptionnable *opt)
  {
    if (UsedOptions.count(ArgName))
      throw OptionsException("Options::AddOptions : fonction ajoutee deux fois");
    else
      UsedOptions.insert(std::pair<std::string, IOptionnable *> (ArgName, opt));
  }

  void  Options::ReadArg(int argc, char *argv[])
  {
    Arguments   listarg;
    ReadArg(argc, argv, listarg);
  }

  void      Options::ReadArg(int argc, char *argv[], Arguments &listString)
  {
    MapOptions::iterator        it;
    std::string                 command;
    int                         idx = 1;

    listString.clear();
    command.reserve(4096);
    while (idx < argc)
      {
        command = argv[idx];
        it = UsedOptions.find(command);
        // Si l'option existe dans la liste des options
        if (it != UsedOptions.end())
          {
            Arguments   listArg;

            ++idx;
            for (int i = 0; i < it->second->NbArg; ++i)
              {
                if (i + idx >= argc)
                  {
                    throw OptionsException("Argument manquant :" + it->first);
                  }
                listArg.push_back(argv[i + idx]);
              }
            it->second->Execute(listArg);
            idx += it->second->NbArg;
          }
        // Ou si elle est egale a l'argument stoppant
        else
          if (command.compare(_stoppingArg) == 0)
            {
              listString.clear();
              for (++idx; idx < argc; ++idx)
                {
                  listString.push_back(argv[idx]);
                }
              return;
            }
        // Si il faut s'arrete au premier argument faux
          else
            if (_stopAtFirstUnknowArg == true)
              {
                std::string str(argv[idx]);
                PrintHelp();

                if (str.compare("--help") == 0 || str.compare("-h") == 0)
                  throw OptionsException("Aide");
                else
                  throw OptionsException("Argument invalide : " + std::string(argv[idx]));
              }
        // Si on ne s'arrete pas au premier argument faux
            else
              {
                listString.push_back(argv[idx]);
                ++idx;
              }
      }
    return;
  }

  void  Options::SetStoppingArg(const std::string &str)
  {
    _stoppingArg = str;
  }

  void  Options::PrintHelp() const
  {
    MapOptions::const_iterator    cit;
    std::size_t                   maxsize = 0;

    if (_progName.size() > 0)
      std::cout << _progName << std::endl;
    std::cout << "Usage :" << std::endl;
    for (cit = UsedOptions.begin(); cit != UsedOptions.end(); cit++)
      {
        if (cit->first.size() > maxsize)
          maxsize = cit->first.size();
      }
    for (cit = UsedOptions.begin(); cit != UsedOptions.end(); cit++)
      {
        std::size_t size = cit->first.size();

        std::cout << cit->first;
        for (std::size_t i = size; i < maxsize; i++)
          std::cout << " ";
        std::cout << " : " << cit->second->HelpMsg << std::endl;
      }
  }
}
