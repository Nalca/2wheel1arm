#include "Compat.hpp"

#ifdef OS_LINUX

#include <unistd.h>

namespace tool
{
  void  msleep(unsigned int msecond)
  {
    if (msecond > 1000)
    {
      unsigned int sec = msecond / 1000;
      msecond %= 1000;
      sleep(sec);
    }

    usleep(msecond * 1000);
  }
}

#elif defined OS_WINDOWS

#include <Windows.h>

namespace tool
{
  void  msleep(unsigned int msecond)
  {
    Sleep(static_cast<DWORD>(msecond));
  }
}

#endif