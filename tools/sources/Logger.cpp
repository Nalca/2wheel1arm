#include <cassert>
#include "Logger.hpp"
#include "Macro.hpp"

Logger *Logger::logPtr = nullptr;
const Logger::LogLevel Logger::Message = 1;
const Logger::LogLevel Logger::Warning = 2;
const Logger::LogLevel Logger::Error = 3;


Logger::Logger()
{
  OstreamMap[Message] = new LoggerStream(&(std::cout), "[Message] : ", Color::green, Color::origine, false);
  OstreamMap[Warning] = new LoggerStream(&(std::cout), "[Warning] : ", Color::yellow, Color::origine, false);
  OstreamMap[Error] = new LoggerStream(&(std::cerr), "[Erreur] : ", Color::red, Color::origine, false);
}

Logger::~Logger()
{
  std::map<LogLevel, LoggerStream *>::iterator it;

  logPtr = nullptr;
  for (it = OstreamMap.begin(); it != OstreamMap.end(); it++)
    {
      delete it->second;
    }
}

void  Logger::message(LogLevel lvl, const std::string &msg)
{
  Logger::message(lvl, msg.c_str());
}

void    Logger::message(LogLevel lvl, const char *msg)
{
  std::map<LogLevel, LoggerStream *>::iterator it;
  assert(logPtr != nullptr);

  if ((it = logPtr->OstreamMap.find(lvl)) != logPtr->OstreamMap.end())
    it->second->message(msg);
  else
    {
      std::cerr << __func__ << "Log level " << lvl << " n'existe pas" << std::endl;
      abort();
    }
}

void    Logger::setPrefix(LogLevel lvl, const std::string &Prefix,
                          Color::Color colorPrefix, Color::Color colorMsg)
{
  std::map<LogLevel, LoggerStream *>::iterator it;
  assert(logPtr != nullptr);

  if ((it = logPtr->OstreamMap.find(lvl)) != logPtr->OstreamMap.end())
    it->second->setAttribut(Prefix, colorPrefix, colorMsg);
  else
    {
      std::cerr << __func__ << "Log level " << lvl << " n'existe pas" << std::endl;
      abort();
    }
}

void    Logger::open()
{
  assert(Logger::logPtr == nullptr);
  logPtr = new Logger();
}

void    Logger::open(LogLevel level, std::ostream *streamptr, bool DeleteAtClose)
{
  if (logPtr == nullptr)
    Logger::open();
  std::map<LogLevel, LoggerStream *>::iterator it = logPtr->OstreamMap.find(level);

  if (it != logPtr->OstreamMap.end())
    {
      std::cerr << __func__ << " : level " << level << " deja existant." << std::endl;
      abort();
    }
  else
    logPtr->OstreamMap[level] = new LoggerStream(streamptr, "", Color::origine, Color::origine, DeleteAtClose);
}

void    Logger::close()
{
  assert(logPtr != nullptr);
  delete logPtr;
  logPtr = nullptr;
}

void    Logger::close(LogLevel level)
{
  assert(logPtr != nullptr);
  std::map<LogLevel, LoggerStream *>::iterator it = logPtr->OstreamMap.find(level);

  if (it == logPtr->OstreamMap.end())
    {
      std::cerr << __func__ << "Loglevel " << level << " n'existe pas." << std::endl;
      abort();
    }
  else
    {
      if (it->second->DeleteAtClose)
        delete it->second;
      logPtr->OstreamMap.erase(it);
    }
}

Logger::LoggerStream::LoggerStream(std::ostream *_os,
                                   const std::string &_prefixTxt,
                                   Color::Color _prefixColor,
                                   Color::Color _textColor,
                                   bool _DeleteAtClose)
  : outputStream(_os), prefixText(_prefixTxt), prefixColor(_prefixColor), textColor(_textColor),
    DeleteAtClose(_DeleteAtClose)
{
}

Logger::LoggerStream::~LoggerStream()
{
  if (DeleteAtClose)
    delete outputStream;
}

void Logger::LoggerStream::message(const char *str)
{
  assert(outputStream != nullptr);
  if (outputStream->good())
    *outputStream << prefixColor << prefixText << Color::origine << textColor << str << Color::origine << std::endl;
}

void  Logger::LoggerStream::setAttribut(const std::string &_prefixText, Color::Color _prefixColor, Color::Color _textColor)
{
  prefixText = _prefixText;
  prefixColor = _prefixColor;
  textColor = _textColor;
}
