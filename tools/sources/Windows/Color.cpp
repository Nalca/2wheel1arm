#include <windows.h>
#include <iostream>
#include <cstdio>
#include "Color.hpp"

namespace Color
{
  static bool _useColor = true;

  void    useColor(bool value)
  {
    _useColor = value;
  }

#ifndef DOXYGEN_SHOULD_SKIP_THIS
  /*!
   * @internal
   * @class Wcolor
   * @brief Une class qui contient les infos de la console (pour Windows)
   * @var Wcolor *_terminfo
   * @brief Le pointeur sur Wcolor
   */
 class Wcolor
  {
  public:
    Wcolor(): hConsole(GetStdHandle(STD_OUTPUT_HANDLE))
    {
      CONSOLE_SCREEN_BUFFER_INFO consoleInfo;

      GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
      _defaultAttribut = consoleInfo.wAttributes;
    };

    ~Wcolor() {};

    HANDLE hConsole;
    WORD _defaultAttribut;
  };

  Wcolor  *_terminfo = nullptr;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
}

std::ostream    &operator<< (std::ostream &os, const Color::Color &color)
{
  if (Color::_terminfo == nullptr)
    Color::_terminfo = new Color::Wcolor();

  if (Color::_useColor)
    switch (color)
      {
        case Color::white:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
          break;

        case Color::red:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY);
          break;

        case Color::green:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
          break;

        case Color::yellow:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
          break;

        case Color::violet:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
          break;

        case Color::blue:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
          break;

        case Color::origine:
          SetConsoleTextAttribute(Color::_terminfo->hConsole, Color::_terminfo->_defaultAttribut);
          break;

        default
            :
          break;
      }
  return (os);
}

