#include "Color.hpp"

namespace Color
{
  static bool _useColor = true;

  void    useColor(bool value)
  {
    _useColor = value;
  }
}

std::ostream    &operator<< (std::ostream &os, const Color::Color &color)
{
  if (Color::_useColor)
    switch (color)
      {
        case Color::white:
          os << "\033[01;37m";
          break;

        case Color::red:
          os << "\033[01;31m";
          break;

        case Color::green:
          os << "\033[01;32m";
          break;

        case Color::yellow:
          os << "\033[01;33m";
          break;

        case Color::violet:
          os << "\033[01;35m";
          break;

        case Color::blue:
          os << "\033[01;34m";
          break;

        case Color::origine:
          os << "\033[00m";
          break;

        default
            :
          break;
      }
  return (os);
}
