#include <algorithm>
#include "Conversion.hpp"

void    Conversion::IntToBase(unsigned int value, const std::string &base, std::string &buffer)
{
  if ((value / base.size()) != 0)
    IntToBase(value / base.size(), base, buffer);
  buffer += base[value % base.size()];
}

std::string Conversion::IntToBase(unsigned int value, const std::string &base)
{
  std::string   buffer;

  IntToBase(value, base, buffer);
  return (buffer);
}

bool  Conversion::ToBool(std::string str)
{
  std::transform(str.begin(), str.end(), str.begin(), toupper);

  if (str.compare("1") == 0 || str.compare("TRUE") == 0)
    return (true);
  else
    return (false);
}
