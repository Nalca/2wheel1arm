#include <fstream>
#include "Commander.hpp"

/// \todo Le fait de pouvoir executer un individu precis (parmis les resultats)
int main(int argc, char *argv[])
{
  Commander sh("$:");

  try
  {
    sh.FillParam();
    sh.initOneTimeRobot();
    sh.initOneTimeAlgogen();
    sh.addFunc();

    if (argc > 1)
    {
      std::string   filename(argv[1]);
      std::fstream  fs;

      fs.open(filename);
      if (fs.good())
      {
        sh.Run(fs, false);
        sh.Run();
      }
      else
      {
        std::cerr << "Impossible d'utiliser " << filename << ". Utilisation du mode console." << std::endl;
        sh.Run();
      }
    }
    else
      sh.Run();
    return (EXIT_SUCCESS);
  }

  catch (const BasicException except)
  {
    std::cerr << "Erreur critique : " << except.what() << std::endl;
    return (EXIT_FAILURE);
  }
}