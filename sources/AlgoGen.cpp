#include <cstdlib>
#include <ctime>
#include <cassert>
#include <stdexcept>
#include "Compat.hpp"
#include "Conversion.hpp"
#include "AlgoGen.h"
#include "Color.hpp"
#include "Chromosome.h"
#include "Param.hpp"
#include "Fitness/Fitness.hpp"
#include "Robot.h"
#include "Logger.hpp"


const AlgoParam  *AlgoGen::param;

AlgoGen::AlgoGen(const AlgoParam *_param, ApiBlli::Robot *_robot) : robot(_robot)
{
  AlgoGen::param = _param;
}

AlgoGen::~AlgoGen()
{
  std::map<std::string, IFitness *>::iterator it;

  for (it = FitnessMap.begin(); it != FitnessMap.end(); it++)
    delete it->second;
  DeletePopulation(pop_Parent);
  DeletePopulation(pop_Child);
  DeletePopulation(pop_NextGen);
  AlgoGen::param = nullptr;
}

void  AlgoGen::Run()
{
  Tpop  &parent = this->pop_Parent;
  Tpop  &child = this->pop_Child;
  Tpop  &nextgen  = this->pop_NextGen;

  TestConfig();
  ResetGenerationCounter();
  DeletePopulation(parent);
  GenerateRandomPopulation(parent);
  while (GetCurrentGeneration() <= param->NbGenerationBeforeStop)
    {
      EvaluatePopulation(parent);
      if (param->UsePopIntermediare)
      {
        for (std::map<std::string, IFitness *>::iterator it = FitnessMap.begin(); it != FitnessMap.end(); it++)
            it->second->EvaluateGeneration(this);
        SelectionAndReproduction(child);
        EvaluatePopulation(child);
      }
      for (std::map<std::string, IFitness *>::iterator it = FitnessMap.begin(); it != FitnessMap.end(); it++)
        it->second->EvaluateGeneration(this);
      SelectionAndReproduction(nextgen);
      GotoNextGeneration();
    }
  FitnessShowBest();
}

void  AlgoGen::TestConfig()
{
  std::cout << std::endl << std::endl;
  std::cout << "Configuration : " << std::endl;
  std::cout << "   Real-Time multiplieur (Marilou)                         : " << Color::white << param->RealTimeMultiplier << Color::origine << std::endl;
  std::cout << "   Nombre de generations avant selection finale            : " << Color::white << param->NbGenerationBeforeStop << Color::origine << std::endl;
  std::cout << "   Taille de la population                                 : " << Color::white << param->SizePopulation << Color::origine << std::endl;
  std::cout << "   Pourcentage de population aleatoire a chaque generation : " << Color::white << param->Pct_GenerationAleatoire << "%" << Color::origine << std::endl;
  std::cout << " --- " << std::endl;
  std::cout << "   Utilisation d'une population intermediare               : " << Color::white << (param->UsePopIntermediare ? "Oui" : "Non") << Color::origine << std::endl;
  std::cout << "   Nombre de fitness utilisees                             : " << Color::white << this->FitnessMap.size() << Color::origine << std::endl;
  std::cout << " --- " << std::endl;
  std::cout << "   Nombre de sequences dans un chromosome                  : " << Color::white << param->NbSequenceChromosome << Color::origine << std::endl;
  std::cout << "   Nombre de repetition du chromosome                      : " << Color::white << param->NbRepetition << Color::origine << std::endl;
  std::cout << "   Pourcentage de mutation d'un chromosome                 : " << Color::white << param->PctMutationChromosome << "%" << Color::origine << std::endl;
  std::cout << "   Strategie Crossover                                     : " << Color::white;

  switch (param->CrossOver_Strategy)
  {
    case OnePoint:
      std::cout << "OnePoint";
      break;

    case Uniform:
      std::cout << "Uniform";
      break;

    default:
      std::cout << "Inconnue (Invalide)";
      break;
  }
  std::cout << Color::origine << std::endl << std::endl;

  // Les tests sur les valeurs de param sont fait ici
  if (!(param->SizePopulation > 0))
    throw AlgogenException("SizePopulation invalide.");
  if (!(param->NbGenerationBeforeStop >= 1))
    throw AlgogenException("NbGenerationBeforeStop invalide.");

  if (!(param->RealTimeMultiplier >= 1))
    throw AlgogenException("RealTimeMultiplier invalide.");
  if (!(param->Pct_GenerationAleatoire <= 100))
    throw AlgogenException("Pct_GenerationAleatoire invalide.");

  if (!(param->Pct_Cryo <= 100))
    throw AlgogenException("Pct_Cryo invalide");
  if (!(param->NbSequenceChromosome >= 1))
    throw AlgogenException("NbSequenceChromosome invalide.");

  if (!(param->NbRepetition > 0))
    throw AlgogenException("NbRepetition invalide.");
  if (!(param->PctMutationChromosome <= 100))
    throw AlgogenException("PctMutationChromosome invalide.");

  if (!(param->CrossOver_Strategy == OnePoint || param->CrossOver_Strategy == Uniform))
    throw AlgogenException("Crossover Strategie invalide.");
  if (!(this->FitnessMap.size() > 0))
    throw AlgogenException("Aucune fitness utilisee, impossible d'evaluer les robots.");

}

void     AlgoGen::FitnessAdd(const std::string &name, std::size_t FitPop, std::size_t FitCryoPop)
{
  if (FitnessMap.count(name))
    throw FitnessException("La fitness " + name + " est deja enregistree.");
  else
  {
    IFitness  *fit = IFitness::GetFitness(name, FitPop, FitCryoPop);

    if (fit == nullptr)
      throw FitnessException("La fitness " + name + " n'existe pas.");
    FitnessMap[name] = fit;
  }
}

void     AlgoGen::FitnessRemove(const std::string &name)
{
  std::map<std::string, IFitness *>::iterator it;

  if ((it = FitnessMap.find(name)) == FitnessMap.end())
    throw FitnessException("La fitness " + name + " n'est pas enregistree.");
  FitnessMap.erase(it);
}

const IFitness *AlgoGen::FitnessIsUsed(const std::string &name) const
{
  std::map<std::string, IFitness *>::const_iterator cit;

  if ((cit = FitnessMap.find(name)) == FitnessMap.end())
    return (nullptr);
  else
    return (cit->second);
}

void  AlgoGen::EvaluteIndividu(const Individu *testIndividual, std::list<Individu *> &result, std::size_t nbEval)
{
  DeletePopulation(result);
  std::cout << "Evaluation speciale d'un individu : " << Color::yellow << testIndividual->GetChromosome().ToString() << Color::origine << std::endl;
  std::cout << Color::red;
  for (std::size_t i = 0; i < nbEval; i++)
  {
    Individu  *clone = Individu::Clone(testIndividual, false);

    this->EvaluteIndividu(clone);
    std::cout << "." << std::flush;
    result.push_back(clone);
  }
  std::cout << Color::origine << std::endl;
}

void    AlgoGen::EvaluteIndividu(Individu *testIndividual, std::size_t nbRepetition)
{
  robot->Init();
  Sleep(1000);
  testIndividual->AddPosition(robot->GetPosition());

  if (nbRepetition == 0)
    nbRepetition = param->NbRepetition;
  
  for (std::size_t nbr = 0; nbr < nbRepetition; nbr++)
  {
    for (unsigned int i = 0; i < AlgoGen::param->NbSequenceChromosome; i++)
      {
        robot->SetIndex(ApiBlli::Leg::FINGER, testIndividual->GetChromosome().GetValue(Chromosome::FINGER, i), 10);
        robot->SetIndex(ApiBlli::Leg::ARM, testIndividual->GetChromosome().GetValue(Chromosome::ARM, i), 10);
        robot->SetIndex(ApiBlli::Leg::SHOULDER, testIndividual->GetChromosome().GetValue(Chromosome::SHOULDER, i), 10);
        tool::msleep(testIndividual->GetChromosome().GetValue(Chromosome::TIMER, i) / param->RealTimeMultiplier);
      }
    testIndividual->AddPosition(robot->GetPosition());
  }
}

void    AlgoGen::GenerateRandomPopulation(std::list<Individu *> &emptyPopulation)
{
  while (emptyPopulation.size() < param->SizePopulation)
    {
      Individu *ptr = Individu::GetNewInstance();
      ptr->SetChromosome(Chromosome::GetRandom());
      emptyPopulation.push_back(ptr);
    }
}

void    AlgoGen::EvaluatePopulation(std::list<Individu *> &testPopulation)
{
  std::list<Individu *>::iterator it;
  std::size_t                     i = 0;

  for (it = testPopulation.begin(); it != testPopulation.end(); it++)
    {
      ++i;
      std::cout << "Individu " << i << std::endl;
      EvaluteIndividu(*it);
      for (std::map<std::string, IFitness *>::iterator fit = FitnessMap.begin(); fit != FitnessMap.end(); fit++)
        fit->second->EvaluateIndividual(*it);
    }
}

void  AlgoGen::SelectionAndReproduction(Tpop &popTarget)
{
  std::map<std::string, IFitness *>::iterator it;
  DeletePopulation(popTarget);

  // On prend le meilleur de la fitness
  it = FitnessMap.begin();
  while (popTarget.size() < param->SizePopulation && it != FitnessMap.end())
  {
    Individu *ptr = it->second->GetCloneBestIndividual(IFitness::CRYO, 0);
    if (ptr)
      popTarget.push_back(ptr);
    it++;
  }

  // On prend le meilleur de la génération de la fitness
  it = FitnessMap.begin();
  while (popTarget.size() < param->SizePopulation && it != FitnessMap.end())
  {
    Individu *ptr = it->second->GetCloneBestIndividual(IFitness::GENERATION, 0);
    if (ptr)
      popTarget.push_back(ptr);
    it++;
  }
  
  // On genere la pop random
  if (param->Pct_GenerationAleatoire > 0)
  {
    std::size_t nbInd = static_cast<std::size_t>(((float)param->SizePopulation * (float)param->Pct_GenerationAleatoire / 100.0));
    std::size_t i = 0;

    while (i < nbInd && popTarget.size() < param->SizePopulation)
    {
      popTarget.push_back(Individu::GetNewInstance(Chromosome::GetRandom()));
      ++i;
    }
  }


  // On prend un des meilleurs en random
  it = FitnessMap.begin();
  while (popTarget.size() < param->SizePopulation && it != FitnessMap.end())
  {
    Individu *ptr = it->second->GetCloneRandomIndividual(param->Pct_Cryo);
    if (ptr)
      popTarget.push_back(ptr);
    it++;
  }

  // On prend un des meilleurs en random ET on le fait muter
  it = FitnessMap.begin();
  while (popTarget.size() < param->SizePopulation && it != FitnessMap.end())
  {
    Individu  *ptr = it->second->GetCloneRandomIndividual(param->Pct_Cryo);
    if (ptr)
    {
      Chromosome::Mutation(ptr->GetChromosome());
      popTarget.push_back(ptr);
    }
    it++;
  }

  // On fait une passe a prendre le meilleur de la gen + un bon de la gen (au hasard) + mutation
  it = FitnessMap.begin();
  while (popTarget.size() < param->SizePopulation && it != FitnessMap.end())
  {
    if (it->second->CanReturnIndividual(IFitness::GENERATION))
    {
      const Individu *ind1 = it->second->GetBestIndividual(IFitness::GENERATION);
      const Individu *ind2 = it->second->GetRandomIndividual(param->Pct_Cryo);

      assert(ind2 != nullptr);
      if (ind1 == nullptr)
			ind1 = it->second->GetRandomIndividual(param->Pct_Cryo);
      assert(ind1 != nullptr);
      
      Chromosome *result = Chromosome::CrossOver(ind1->GetChromosome(),
                                                ind2->GetChromosome(),
                                                0.5, param->CrossOver_Strategy);
      Chromosome::Mutation(result);
      popTarget.push_back(Individu::GetNewInstance(result));
    }
    it++;
  }

  // Pour avoir acces aux Fitness en random
  std::vector<IFitness *> vect;
  vect.reserve(FitnessMap.size());
  for (it = FitnessMap.begin(); it != FitnessMap.end(); it++)
  {
    if (it->second->CanReturnIndividual())
      vect.push_back(it->second);
  }

  // On prend du random des (fitness au hasard), on les croise + mutation
  if (vect.size() > 0) // Si y'a des individus
  {
    while (popTarget.size() < param->SizePopulation)
    {
      const Individu  *ind1 = vect[rand() % vect.size()]->GetRandomIndividual(param->Pct_Cryo);
      const Individu  *ind2 = vect[rand() % vect.size()]->GetRandomIndividual(param->Pct_Cryo);

      if (Individu::CompareChromosome(ind1, ind2) == true)
      {
          Chromosome  *result = Chromosome::CrossOver(ind1->GetChromosome(), ind2->GetChromosome(), 0.5, param->CrossOver_Strategy);
          Chromosome::Mutation(result);
          popTarget.push_back(Individu::GetNewInstance(result));
      }
    }
  }
  else // Ils sont tous trop cons pour la fitness (aka, y'en a AUCUN dedans)
  {
    std::cout << "Generation random de la population car aucun individu ne correspond a la fitness." << std::endl;
    this->GenerateRandomPopulation(popTarget);
  }
}

void  AlgoGen::GotoNextGeneration()
{
  DeletePopulation(pop_Parent);
  DeletePopulation(pop_Child);
  pop_NextGen.swap(pop_Parent);

  for (std::map<std::string, IFitness *>::iterator it = FitnessMap.begin(); it != FitnessMap.end(); it++)
  {
    std::stringstream           ss;
    const std::list<Individu *> &genPop = it->second->GetGenPop();

    for (std::list<Individu *>::const_iterator cit = genPop.begin(); cit != genPop.end(); cit++)
    {
      ss << (*cit)->GetChromosome().ToString() << ";" << (*cit)->GetDistanceTotale().x << ";" << this->currentGeneration << std::flush;
      Logger::message(5, ss.str());
      ss.clear();
      ss.str("");
    }
    it->second->DeleteGenerationResult();
  }
  Logger::message(5, "");
  ++currentGeneration;
  std::cout << "Passage a la generation " << currentGeneration << std::endl;
}

void  AlgoGen::FitnessShowBest() const
{
  std::map<std::string, IFitness *>::const_iterator cit;

  for (cit = FitnessMap.begin(); cit != FitnessMap.end(); cit++)
  {
    const Individu *best = cit->second->GetBestIndividual(IFitness::CRYO);

    std::cout << best->GetChromosome().ToString() << std::endl;
    Logger::message(LOG_LEVEL_BEST, best->GetChromosome().ToString() + ";" + Conversion::ToString<float>(best->GetDistanceTotale().x));
  }
}

// Utiles -------------------------------------------------------------------------------
void            AlgoGen::ResetGenerationCounter()
{
  currentGeneration = 0;
}

std::size_t    AlgoGen::GetCurrentGeneration() const
{
  return (this->currentGeneration);
}


void    AlgoGen::DeletePopulation(Tpop &popToKill)
{
  std::list<Individu *>::iterator it;
/*
  for (it = popToKill.begin(); it != popToKill.end(); it++)
    delete *it;
*/
  popToKill.clear();
}
