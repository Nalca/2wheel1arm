#include <stdexcept>
#include <sstream>
#include <cassert>
#include "AlgoGen.h"
#include "Color.hpp"
#include "Conversion.hpp"
#include "Leg.h"
#include "Macro.hpp"

Individu::~Individu()
{
  if (algo)
    delete algo;
  delete dist;
  DELETE_VECTOR(ApiBlli::Position*, positions);
}

Individu::Individu() : algo(nullptr)
{
  dist = new ApiBlli::Position();
}

Individu    *Individu::GetNewInstance(Chromosome *newChromosome)
{
  Individu *ptr = new Individu();
  ptr->SetChromosome(newChromosome);
  return (ptr);
}

Individu    *Individu::Clone(const Individu *target, bool withPositions)
{
  if (withPositions)
  {
    Individu *ind = GetNewInstance(Chromosome::Clone(target->GetChromosome()));
    ind->copyPositions(target);
    ind->dist->equal(target->dist);
    return (ind);
  }
  else
  {
    Individu  *ind = GetNewInstance(Chromosome::Clone(target->GetChromosome()));
    return (ind);
  }
}

Individu  *Individu::Clone(const Individu &target, bool withPositions)
{
  return (Individu::Clone(&target, withPositions));
}

const ApiBlli::Position &Individu::GetDistanceTotale() const
{
  return (*dist);
}

void    Individu::AddPosition(ApiBlli::Position *position)
{
  assert(position != nullptr);
  positions.push_back(position);
  dist->x = position->x - positions.front()->x;
  dist->y = position->y - positions.front()->y;
  dist->z = position->z - positions.front()->z;
}

const std::vector<ApiBlli::Position *>  &Individu::GetPositions() const
{
  return (positions);
}

bool  Individu::CompareChromosome(const Individu *ind1, const Individu *ind2)
{
  return (ind1->GetChromosome() == ind2->GetChromosome());
}

void    Individu::SetChromosome(Chromosome *newAlgo)
{
  algo = newAlgo;
}

void    Individu::copyPositions(const Individu *target)
{
  std::vector<ApiBlli::Position *>::const_iterator cit;

  positions.reserve(target->positions.size());
  DELETE_VECTOR(ApiBlli::Position*, positions);
  for (cit = target->positions.begin(); cit != target->positions.end(); cit++)
    this->positions.push_back(new ApiBlli::Position(*(*cit)));
}

// -------------------------------- Partie information -------------------------
std::string    intToString(int value, Chromosome::StringType type)
{
  std::stringstream    ss;

  switch (type)
    {
      case Chromosome::DECIMAL:
      {
        ss.width(4);
        ss << value;
        return ss.str();
      }

      case Chromosome::HEXA:
      {
        std::string    str;
        Conversion::IntToBase(value, "0123456789ABCDEF", str);
        ss.width(3);
        ss << str;
        return ss.str();
      }

      default:
        throw std::runtime_error("");
    }
}

void    Individu::ShowChromosomeDiff(const Individu *other, std::ostream &os, Chromosome::StringType type) const
{
  Color::useColor(os == std::cout);

  for (unsigned int i = 0; i < AlgoGen::param->NbSequenceChromosome; i++)
    {
      int valueSelf;
      int valueOther;

      valueSelf = this->GetChromosome().GetValue(Chromosome::FINGER, i);
      valueOther = other->GetChromosome().GetValue(Chromosome::FINGER, i);
      if (valueSelf == valueOther)
        os << intToString(valueSelf, type) << "-";
      else
        os << Color::yellow << intToString(valueSelf, type) << Color::origine << "-";


      valueSelf = this->GetChromosome().GetValue(Chromosome::ARM, i);
      valueOther = other->GetChromosome().GetValue(Chromosome::ARM, i);
      if (valueSelf == valueOther)
        os << intToString(valueSelf, type) << "-";
      else
        os << Color::yellow << intToString(valueSelf, type) << Color::origine << "-";


      valueSelf = this->GetChromosome().GetValue(Chromosome::SHOULDER, i);
      valueOther = other->GetChromosome().GetValue(Chromosome::SHOULDER, i);
      if (valueSelf == valueOther)
        os << intToString(valueSelf, type) << "-";
      else
        os << Color::yellow << intToString(valueSelf, type) << Color::origine << "-";


      valueSelf = this->GetChromosome().GetValue(Chromosome::TIMER, i);
      valueOther = other->GetChromosome().GetValue(Chromosome::TIMER, i);
      if (valueSelf == valueOther)
        os << intToString(valueSelf, type);
      else
        os << Color::yellow << intToString(valueSelf, type) << Color::origine;
      os << " | ";
    }
  os << std::endl;
}

// ---------------------------------------- Partie Get --------------------------

const Chromosome        &Individu::GetChromosome() const
{
  assert(algo != nullptr);
  return (*algo);
}

Chromosome        &Individu::GetChromosome()
{
  assert(algo != nullptr);
  return (*algo);
}
/*
void  Individu::log(std::ostream &os)
{
  if (os.good())
    {
      os << this->algo->ToString() << ";";
      for (std::vector<ApiBlli::Position>::const_iterator cit = positions.begin(); cit != positions.end(); cit++)
        {
          os << cit->x << ";";
        }
      os << std::endl;
    }
}
*/

bool  Individu::operator==(const Individu &other) const
{
  return (this->GetChromosome() == other.GetChromosome());
}