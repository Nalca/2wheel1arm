#include <algorithm>
#include "Commander.hpp"
#include "Conversion.hpp"
#include "AlgoGen.h"
#include "ApiBlli.h"
#include "Logger.hpp"

Commander::Commander(const char *prompt) : Shell(prompt), algogen(nullptr), robot(nullptr), param(nullptr), logIsOpened(false)
{
  Logger::open();
}

Commander::~Commander()
{
  Logger::close();
  delete algogen;
  delete robot;
  delete param;
}

void            Commander::addFunc()
{
  this->AddFuncPtr(Shell::Funcptr_externe::get(&add_fitness, this->algogen, "add_fitness",
                "Ajoute une fonction de fitness au set de fitness utilise.",
                "[FitnessName] ([FitPop] [FitCryoPop])"));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&remove_fitness, this->algogen, "remove_fitness",
                "Supprime une fonction de fitness du set de fitness utilise.",
                "[FitnessName]"));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&list_fitness, this->algogen, "list_fitness",
                "Liste les fonctions de fitness qui seront utilisees.", ""));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&start, this, "start",
                "Lance la simulation avec les parametres en environnement", ""));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&use_default_conf, this, "use_default_conf",
                "Creer des parametres par default (override pour ecraser ceux existant).",
                "(\"override\")"));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&runseq, this->algogen, "runseq",
                "Utilise le meilleur individu pour avancer (100 iterations par default).",
                "[FitnessName] (nb iteration)"));
  this->AddFuncPtr(Shell::Funcptr_externe::get(&DNAfromfile, this, "DNAfromfile",
                "Execute un liste de s�quence d'ADN � partir d'un fichier (une s�quence par ligne).",
                "[file]"));
}

// cast CrossOverStrategy -----
CrossOverStrategy CrossOverStrategy_FromString(std::string str)
{
  std::transform(str.begin(), str.end(), str.begin(), toupper);

  if (str.compare("ONEPOINT") == 0)
    return (CrossOverStrategy::OnePoint);
  else if (str.compare("UNIFORM") == 0)
    return (CrossOverStrategy::Uniform);
  else
    throw Shell::ShellException("Cast invalide (" + std::string(__func__) + ") : " + str);
}

void  Commander::FillParam()
{
  if (GetEnvSize() == 0)
    {
      std::list<std::string>  listArg;
      std::cout << "Using default conf" << std::endl;
      use_default_conf(this, listArg);
    }

  // Gestion du logger
  if (logIsOpened == false)
  {
    std::string exe_filename = GetEnvVar("LOG_EXE_NAME");
    std::string best_filename = GetEnvVar("LOG_BEST_FNAME");

    std::fstream *exe = new std::fstream;
    std::fstream *best = new std::fstream;

    exe->open(exe_filename, std::ios_base::out | std::ios_base::trunc);
    best->open(best_filename, std::ios_base::out | std::ios_base::trunc);
    if (exe->good() && best->good())
    {
      Logger::open(LOG_LEVEL_EXE, exe, true);
      Logger::open(LOG_LEVEL_BEST, best, true);
      logIsOpened = true;
    }
    else
    {
      delete exe;
      delete best;
      Logger::message(Logger::Error, "Impossible d'ouvrir les fichiers " + exe_filename + " et " + best_filename);
    }
  }

  // Gestion de param
  if (param == nullptr)
    param = new AlgoParam;
  param->UsePopIntermediare = Conversion::ToBool(GetEnvVar("USE_POPINTERMEDIAIRE"));
  param->SizePopulation = Conversion::FromString<std::size_t>(GetEnvVar("SIZE_POPULATION"));
  param->NbRepetition = Conversion::FromString<std::size_t>(GetEnvVar("NB_REPETITION"));
  param->RealTimeMultiplier = Conversion::FromString<std::size_t>(GetEnvVar("REALTIME_MULTIPLIER"));
  param->Pct_GenerationAleatoire = Conversion::FromString<std::size_t>(GetEnvVar("PCT_GENERATIONALEATOIRE"));
  param->Pct_Cryo = Conversion::FromString<std::size_t>(GetEnvVar("PCT_CRYO"));
  param->NbSequenceChromosome = Conversion::FromString<std::size_t>(GetEnvVar("NB_SEQUENCES_CHROMOSOME"));
  param->PctMutationChromosome = Conversion::FromString<std::size_t>(GetEnvVar("PCT_MUTATION_CHROMOSOME"));
  param->NbGenerationBeforeStop = Conversion::FromString<std::size_t>(GetEnvVar("NB_GENERATION_BEFORE_STOP"));
  param->CrossOver_Strategy = CrossOverStrategy_FromString(GetEnvVar("CROSSOVER_STRATEGY"));
}

void  Commander::initOneTimeRobot()
{
  if (robot == nullptr)
  {
    ApiBlli::ApiBlli::GetInstance()->Init("localhost");
    std::list<ApiBlli::Robot *> robotList = ApiBlli::ApiBlli::GetInstance()->GetRobots();
    if (robotList.size() != 0)
      {
        robot = robotList.front();
      }
    else
      throw RobotException("No robot found.");
  }
}

void  Commander::initOneTimeAlgogen()
{
  if (algogen == nullptr)
  {
    algogen = new AlgoGen(param, this->getRobot());
  }
}

ApiBlli::Robot  *Commander::getRobot()
{
  if (robot == nullptr)
    throw RobotException("No robot found.");
  return (robot);
}
