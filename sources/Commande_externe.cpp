#include <iostream>
#include <fstream>
#include <exception>
#include "Commander.hpp"
#include "AlgoGen.h"
#include "Logger.hpp"
#include "Fitness/Fitness.hpp"
#include "Conversion.hpp"
#include "Robot.h"

NEW_EXCEPTION(DNAFromFileException, BasicException);

/*!
 *  \internal Argument 1 : Nom de la fitness
 *  \internal Argument 2 : Population de la fitness (x meilleurs)
 *  \internal Argument 3 : Population Congelee de la fitness
 */

void  add_fitness(void *data, const std::list<std::string> &listArg)
{
  AlgoGen *algo = static_cast<AlgoGen *>(data);

  if (listArg.size() == 1 || listArg.size() == 3)
  {
    try
    {
      std::string   name;
      unsigned int  FitPop;
      unsigned int  FitCryoPop;

      if (listArg.size() == 1)
      {
        name = listArg.front();
        FitPop = 5;
        FitCryoPop = 5;
      }
      else
      {
        std::list<std::string>::const_iterator cit;

        cit = listArg.begin();
        name = (*cit);
        cit++;
        FitPop = Conversion::FromString<unsigned int>(*cit);
        cit++;
        FitCryoPop = Conversion::FromString<unsigned int>(*cit);
      }

      algo->FitnessAdd(name, FitPop, FitCryoPop);
    }

    catch (const FitnessException ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }
  else
  {
    std::cout << "Erreurs sur les arguments, voir `help`" << std::endl;
  }
}

// remove_fitness -------------------------------------------------------------------------
void  remove_fitness(void *data, const std::list<std::string> &listArg)
{
  AlgoGen *algo = static_cast<AlgoGen *>(data);

  if (listArg.size() != 1)
    std::cout << "Erreurs sur les arguments, voir `help`" << std::endl;
  else
  {
    try
    {
      algo->FitnessRemove(listArg.front());
    }

    catch (const FitnessException ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }
}

// list_fitness ---------------------------------------------------------------------------
// (voir Fitness/Fitness.cpp)

// use_default_conf  ----------------------------------------------------------------------
void  use_default_conf(void *data, const std::list<std::string> &listArg)
{
  if (listArg.size() == 1 && listArg.front().compare("override") == 0)
  {
    Commander *sh = static_cast<Commander *>(data);

    sh->setenv("USE_POPINTERMEDIAIRE", "true");
    sh->setenv("SIZE_POPULATION", "60");
    sh->setenv("NB_REPETITION", "10");
    sh->setenv("REALTIME_MULTIPLIER", "5");
    sh->setenv("PCT_GENERATIONALEATOIRE", "0");
    sh->setenv("PCT_CRYO", "5");
    sh->setenv("NB_SEQUENCES_CHROMOSOME", "4");
    sh->setenv("PCT_MUTATION_CHROMOSOME", "5");
    sh->setenv("NB_GENERATION_BEFORE_STOP", "100");
    sh->setenv("CROSSOVER_STRATEGY", "Uniform");
    sh->setenv("LOG_EXE_NAME", "LogExe.txt");
    sh->setenv("LOG_BEST_FNAME", "LogBest.csv");
  }
  else if (listArg.size() == 0)
  {
    Commander *sh = static_cast<Commander *>(data);

    if (sh->GetEnvSize() == 0)
    {
      std::list<std::string>  newListArg;

      newListArg.push_back("override");
      use_default_conf(data, newListArg);
    }
    else
      Logger::message(Logger::Warning, std::string(__func__) + " : A implementer");
  }
  else
    std::cout << "Erreurs sur les arguments, voir `help`" << std::endl;
}

// start ----------------------------------------------------------------------------------
void  start(void *data, const std::list<std::string> &listArg)
{
  if (listArg.size() == 0)
  {
    Commander   *sh = static_cast<Commander *>(data);

    try
      {
      sh->FillParam();
      sh->initOneTimeRobot();
      sh->initOneTimeAlgogen();
      sh->algogen->Run();
      }

    catch (const Shell::ShellException except)
      {
        std::cout << except.what() << std::endl;
        std::cout << "Les variables d'environnement sont-elles toutes configurees ?" << std::endl;
      }

    catch (const RobotException except)
      {
        std::cout << "ROBOT IS DEAD : " << except.what() << std::endl;
      }

    catch (const AlgogenException except)
      {
        std::cout << "Erreur bloquante : " << except.what() << std::endl;
      }
  }
  else
    std::cout << "Erreurs sur les arguments, voir `help`" << std::endl;
}

// runseq ---------------------------------------------------------------------------------------
void  runseq(void *data, const std::list<std::string> &listArg)
{
  (void)data;
  (void)listArg;
  std::cout << "A implementer." << std::endl;
}


// DNAfromfile ---------------------------------------------------------------------------------------
static int split(std::vector<std::string> &elements, std::string &string, char sep)
{
	std::string::size_type ptr = string.find(sep);
	
	elements.clear();
	while(ptr != std::string::npos)
	{
		elements.push_back(string.substr(0, ptr));
		string = string.substr(ptr + 1);
		ptr = string.find(sep);
	}
	elements.push_back(string);
	return elements.size();
}

static int hexToInt(std::string &val)
{
	int value;
	std::stringstream ss;

	ss.clear();
	ss.str(std::string());
	ss << std::hex << val;
	ss >> value;
	return (value);
}

static Chromosome *getChromosomeFromDNA(std::string &dna, int nbSequences)
{
	Chromosome					*chr = NULL;
	std::vector<std::string>	sequences;
	std::vector<int>			values_int;

	if (split(sequences, dna, '|') == nbSequences)
	{
		chr = new Chromosome(nbSequences);
		std::vector<std::string>::iterator it = sequences.begin();

		for (; it != sequences.end(); ++it)
		{
			std::vector<std::string>	str_values;

			if (split(str_values, *it, '-') == 4)
			{
				chr->finger.push_back	(hexToInt(str_values[0]));
				chr->arm.push_back		(hexToInt(str_values[1]));
				chr->shoulder.push_back	(hexToInt(str_values[2]));
				chr->timer.push_back	(hexToInt(str_values[3]));
			}
			else
			{
				delete (chr);
				throw DNAFromFileException("Required 4 values foreach chromosomes: " + (*it));
			}
		}
	}
	else
		std::cout << "[Warning] Skip dna: " << dna << std::endl;
	return (chr);
}

void  DNAfromfile(void *data, const std::list<std::string> &listArg)
{
  Commander *sh = static_cast<Commander *>(data);
  if (listArg.size() != 1)
  {
	std::cout << "Un argument est requis (fichier texte avec les ADN)" << std::endl;
  }

  std::list<Chromosome*> chromosomes;
  std::list<std::string>::const_iterator cit = listArg.begin();
  for (; cit != listArg.end(); ++cit)
  {
	std::fstream file(*cit);
	std::string line;
	int i = 0;

	if (!file)
		std::cout << "File " << (*cit) << " does not exist" << std::endl;
	else
	{
		while(!file.eof())
		{
			++i;
			line.clear();
			getline(file, line);

			if (line.size() != 0)
			{
				std::vector<std::string> elements;

				if (split(elements, line, ';') == 3)
				{
					std::cout << "TEST current elment " << i << ": [DNA;score;generation] -> [" << elements[0] << ";" << elements[1] << ";" << elements[2] << "]" << std::endl;
					try
					{
						chromosomes.push_back(getChromosomeFromDNA(elements[0], AlgoGen::param->NbSequenceChromosome));
					}
					catch (const DNAFromFileException e)
					{
						std::cout << e.what() << std::endl;
					}
				}
				else
					std::cout << "[Warning] Skip line " << i << std::endl;
			}
		}
		if (!chromosomes.empty())
		{
			for (std::list<Chromosome*>::const_iterator cit = chromosomes.begin(); cit != chromosomes.end(); ++cit)
			{
        std::cout << (*cit)->ToString() << std::endl;
        
        Individu  *ind = Individu::GetNewInstance(*cit);
        if (chromosomes.size() == 1)
          sh->algogen->EvaluteIndividu(ind, -1);
        else
          sh->algogen->EvaluteIndividu(ind);
				delete ind;
			}
		}
		else
			std::cout << "[Warning] Empty list of chromosomes" << std::endl;
	}
  }
}
