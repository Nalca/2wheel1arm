#include <algorithm>
#include <cassert>
#include "Fitness/Avancement.hpp"
#include "Macro.hpp"
#include "Color.hpp"
#include "Individu.h"
#include "AlgoGen.h"

namespace Fitness
{
  Avancement::Avancement(const std::string &_name, std::size_t _FitPop, std::size_t _FitCryoPop) : IFitness(_name, _FitPop, _FitCryoPop)
  {
  }

  Avancement::~Avancement()
  {
  }

  // operator >
  bool  Avancement::compSupDistance(const Individu *ind1, const Individu *ind2)
  {
    if (ind1->GetDistanceTotale().x > ind2->GetDistanceTotale().x)
      return (true);
    else
      return (false);
  }

  bool  Avancement::lowerBound(const Individu *ind, float meanX)
  {
    if (meanX > ind->GetDistanceTotale().x)
      return (false);
    else
      return (true);
  }

  void  Avancement::EvaluateGeneration(AlgoGen *algo)
  {
    // Avertos pour le cas particulier ou ils deviennent cons et se mettent tous à reculer
    if (genPop.size() == 0 && cryoPop.size() > 0)
    {
      std::cout << "OH MY GAD ILS SONT DEVENUS SUPER CONS MEME LES BONS!" << std::endl;
      return;
    }

    if (cryoPop.size() != 0)
    {
      std::list<Individu *> movList;

	  /// \todo Choisir ceux qui avancent selon un certain arc de cercle, pour éviter les coureurs de cotés
      while (genPop.size() > 0 && compSupDistance(genPop.front(), cryoPop.back()))
      {
        movList.push_front(genPop.front());
        genPop.pop_front();

        // Vu que ça consanguine beaucoup ...
        if (Individu::CompareChromosome(movList.front(), cryoPop.back()))
            continue;

        // Evaluation de l'individu (pour eviter les coups de moule)
        std::list<Individu *>::iterator it;
        std::list<Individu *> indTry;
        algo->EvaluteIndividu(movList.front(), indTry, 20);
        indTry.sort(&compSupDistance);

        // Calcul de la moyenne
        float   meanX = 0;
        std::size_t count = 0;
        {
          std::list<Individu *>::const_iterator cit = indTry.begin();

          while (cit != indTry.end())
          {
            if (TestIndividual(*cit) == true)
            {
              meanX += (*cit)->GetDistanceTotale().x;
              ++count;
            }
            cit++;
          }
          meanX /= indTry.size();
        }

        if (count <= 5)
          std::cout << "Individu foireux détecté" << std::endl;
		// Remplacement si la moyenne est plus grande que le resultat de la cryo
		/// \todo Limiter la selection à un certain 'degré' de déviation (ie, pas plus de 15% par exemple. Pour éviter d'aller loin et pas droit)
		else if (count >= 15 && cryoPop.back()->GetDistanceTotale().x < meanX)
      {
        assert(indTry.size() > 0);
        it = std::lower_bound(indTry.begin(), indTry.end(), meanX, &Avancement::lowerBound);

		if (it == indTry.end()) // petit hack, à cause de lower_bound, car .end() n'est pas déférençable
			InsertIndividuAtBack(CRYO, Individu::Clone(indTry.back(), true));
		else
			InsertIndividuAtBack(CRYO, Individu::Clone(*it, true));
        SortPopulation(CRYO, compSupDistance);
      }
      // Je clean les individus de test
    for (it = indTry.begin(); it != indTry.end(); it++)
      delete *it;
    }
      // Je réinsère les individus dans la population de la génération.
    for (std::list<Individu *>::iterator it = movList.begin(); it != movList.end(); it++)
        genPop.push_back(*it);
    movList.clear();

    // APRES les tests, je trie la génération courante
    SortPopulation(GENERATION, compSupDistance);
  }
  else // Si cryo vide, on push le meilleur de la generation. Sans le tester.
  {
    std::list<Individu *>::iterator it;

    for (it = genPop.begin(); it != genPop.end() && cryoPop.size() < this->FitCryoPop; it++)
      InsertIndividuAtBack(CRYO, Individu::Clone(*it, true));
  }  
}

  // Les plus gros scores sont en haut
  void  Avancement::EvaluateIndividual(const Individu *ind)
  {
    ApiBlli::Position distTotal = ind->GetDistanceTotale();

    std::cout << "\t" << "ADN  : " << ind->GetChromosome().ToString() << std::endl;
    std::cout << "\t" << "Dist : " << distTotal.x << std::endl << std::endl;

    // Si distance negative, va te faire foutre
    if (ind->GetDistanceTotale().x < 0)
      return;

    // Si il recule à un moment donné
    if (this->TestIndividual(ind) == false)
        return;

    // Si la fitness n'est pas pleine
    if (genPop.size() < this->FitPop)
    {
      InsertIndividuAtBack(GENERATION, Individu::Clone(ind, true));
      SortPopulation(GENERATION, &compSupDistance);
    }
    else // Si elle est pleine
    {
      // Si l'individu est meilleur que le plus mauvais de la population
      if (compSupDistance(ind, genPop.back()))
      {
        InsertIndividuAtBack(GENERATION, Individu::Clone(ind, true));
        SortPopulation(GENERATION, &compSupDistance);
      }
    }
  }

  bool    Avancement::TestIndividual(const Individu *ind)
  {
    // Si il recule
    if (ind->GetDistanceTotale().x <= 0)
      return (false);

    // Si il ne fait que avancer.
    const std::vector<ApiBlli::Position *>    &pos = ind->GetPositions();
    std::vector<ApiBlli::Position *>::const_iterator cit = pos.begin();
    std::vector<ApiBlli::Position *>::const_iterator pcit = cit + 1;

    while (pcit != pos.end())
    {
        if ((*cit)->x > (*pcit)->x)
            return (false);
        cit++;
        pcit++;
    }
    return (true);
  }
}
