#include <stdexcept>
#include <cassert>
#include "Fitness/Fitness.hpp"
#include "Fitness/Avancement.hpp"
#include "Individu.h"
#include "AlgoGen.h"

IFitness  *IFitness::GetFitness(const std::string &name, std::size_t FitPop, std::size_t FitCryoPop)
{
  if (name.compare("Avancement") == 0)
    return new Fitness::Avancement(name, FitPop, FitCryoPop);
  else
    return (nullptr);
}

IFitness::IFitness(const std::string &_name, std::size_t _FitPop, std::size_t _FitCryoPop) : FitPop(_FitPop), FitCryoPop(_FitCryoPop), name(_name)
{
  if (FitPop == 0 || FitCryoPop == 0)
    throw FitnessException("Population nulle pour la fitness " + name);
  std::cout << "Creation de la Fitness " << name << "." << std::endl;
}

IFitness::~IFitness()
{
	DeletePopulation(GENERATION);
    DeletePopulation(CRYO);
}

void  IFitness::DeleteGenerationResult()
{
	DeletePopulation(GENERATION);
}

void  IFitness::InsertIndividuAtBack(TypePop pop, Individu *ind)
{
  std::list<Individu *>   &target = (pop == GENERATION ? genPop : cryoPop);
  std::size_t             maxSize = (pop == GENERATION ? FitPop : FitCryoPop);
  std::list<Individu *>::const_iterator cit;

  assert(ind != nullptr);
  for (cit = target.begin(); cit != target.end(); cit++)
    if (Individu::CompareChromosome(ind, *cit) == true)
    {
      delete ind;
      return;
    }
  
  if (target.size() < maxSize)
    target.push_back(ind);
  else
  {
    delete target.back();
    target.pop_back();
    target.push_back(ind);
  }
}

void  IFitness::DeletePopulation(TypePop pop)
{
  std::list<Individu *> &target = (pop == GENERATION ? genPop : cryoPop);
  for (std::list<Individu *>::iterator it = target.begin(); it != target.end(); it++)
    delete *it;
  target.clear();
}

void  IFitness::SortPopulation(IFitness::TypePop pop, IFitness::FuncCmp fct)
{
  std::list<Individu *> &target = (pop == GENERATION ? genPop : cryoPop);

  if (target.size() > 1)
  {
    target.sort(fct);
  }
}

bool  IFitness::CanReturnIndividual() const
{
  if (genPop.size() == 0 && cryoPop.size() == 0)
    return (false);
  return (true);
}

bool  IFitness::CanReturnIndividual(TypePop pop) const
{
  const std::list<Individu *> &target = (pop == GENERATION ? genPop : cryoPop);

  return (target.size() != 0);
}

const Individu  *IFitness::GetInternalRandom(TypePop pop) const
{
  const std::list<Individu *> &target = (pop == GENERATION ? genPop : cryoPop);
  std::list<Individu *>::const_iterator cit = target.begin();
  assert(target.size() != 0);
  std::size_t                           randValue = rand() % target.size();

  for (std::size_t i = 0; i < randValue; i++)
    cit++;
  return (*cit);
}

Individu  *IFitness::GetCloneRandomIndividual(std::size_t PctBest) const
{
	if (this->CanReturnIndividual())
		return (Individu::Clone(GetRandomIndividual(PctBest), false));
	else
		return (nullptr);
}

Individu  *IFitness::GetCloneBestIndividual(TypePop pop, std::size_t num) const
{
	if (this->CanReturnIndividual(pop))
		return (Individu::Clone(GetBestIndividual(pop, num)));
	else
		return (nullptr);
}

const Individu *IFitness::GetRandomIndividual(std::size_t PctBest) const
{
  std::size_t value = rand() % 100;

  if (value >= PctBest)
  {
    if (cryoPop.size() == 0)
    {
      if (genPop.size() == 0)
        return (nullptr);
      else
        return (GetInternalRandom(GENERATION));
    }
    else
      return (GetInternalRandom(CRYO));
  }
  else
  {
    if (genPop.size() == 0)
    {
      if (cryoPop.size() == 0)
        return (nullptr);
      else
        return (GetInternalRandom(CRYO));
    }
    else
      return (GetInternalRandom(GENERATION));
  }
}

const Individu *IFitness::GetBestIndividual(TypePop pop, std::size_t num) const
{
  const std::list<Individu *> &target = (pop == GENERATION ? genPop : cryoPop);
  std::list<Individu *>::const_iterator cit = target.begin();
  std::size_t                           i = 0;
  
  if (target.size() == 0)
	  return (nullptr);
  num = num % target.size();
  while (cit != target.end() && i < num)
  {
    cit++
    ++;
  }
  return (*cit);
}


// -- Pour le mettre a jour en meme temps que les Fitness --
static  void  show_fitness(const std::string &fitname, AlgoGen *algo);
void  list_fitness(void *data, const std::list<std::string> &listArg)
{
  AlgoGen *algo = static_cast<AlgoGen *>(data);

  if (listArg.size() != 0)
  {
    std::cout << "Erreur, A implementer" << std::endl;
  }
  else
  {
    show_fitness("Avancement", algo);
  }
}

// implementation static
static  void  show_fitness(const std::string &fitname, AlgoGen *algo)
{
  const IFitness  *ptr = algo->FitnessIsUsed(fitname);

  std::cout << "[" << (ptr != nullptr ? "x" : " ") << "] " << fitname;
  if (ptr)
    std::cout << " : FitPop=" << ptr->getFitnessPopulation() << ", FitCryoPop=" << ptr->getFitnessCryoPopulation() << std::endl;
  else
    std::cout << std::endl;
  std::cout << std::endl;
}