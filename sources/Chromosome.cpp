#include <sstream>
#include <iostream>
#include <cassert>
#include <stdexcept>
#include "Chromosome.h"
#include "AlgoGen.h"
#include "Conversion.hpp"
#include "Param.hpp"

Chromosome::Chromosome()
{
  finger.resize(AlgoGen::param->NbSequenceChromosome, 0);
  arm.resize(AlgoGen::param->NbSequenceChromosome, 0);
  shoulder.resize(AlgoGen::param->NbSequenceChromosome, 0);
  timer.resize(AlgoGen::param->NbSequenceChromosome, 0);
}

Chromosome::Chromosome(std::size_t sizeVector)
{
  finger.reserve(sizeVector);
  arm.reserve(sizeVector);
  shoulder.reserve(sizeVector);
  timer.reserve(sizeVector);
}

Chromosome::~Chromosome()
{
}

/*!
 * \brief Genere un nouveau chromosome, avec ses valeurs au hasard
 */
Chromosome    *Chromosome::GetRandom()
{
  Chromosome    *newAlgo = new Chromosome();

  for (unsigned int i = 0; i < AlgoGen::param->NbSequenceChromosome; i++)
    {
      newAlgo->finger[i] = Chromosome::GenerateRandomValue(FINGER);
      newAlgo->arm[i] = Chromosome::GenerateRandomValue(ARM);
      newAlgo->shoulder[i] = Chromosome::GenerateRandomValue(SHOULDER);
      newAlgo->timer[i] = Chromosome::GenerateRandomValue(TIMER);
    }
  return (newAlgo);
}

/*!
 * \brief La version avec pointeur
 */
Chromosome    *Chromosome::CrossOver(const Chromosome *first, const Chromosome *second, float CrossPoint, CrossOverStrategy strategy)
{
  assert(first != nullptr);
  assert(second != nullptr);
  return (Chromosome::CrossOver(*first, *second, CrossPoint, strategy));
}

/*!
 * \brief Retourne un nouveau chromosome issue d'un melange des deux chromosomes donnes.
 * \var CrossPoint
 * \brief doit etre comprise entre 0 & 1
 */
Chromosome    *Chromosome::CrossOver(const Chromosome &first, const Chromosome &second, float CrossPoint, CrossOverStrategy strategy)
{
  assert(CrossPoint > 0 && CrossPoint < 1); // Valeur de CrossPoint invalide

  Chromosome      *newAlgo = new Chromosome();
  unsigned int    crossPointBreak = static_cast<unsigned int>(AlgoGen::param->NbSequenceChromosome * CrossPoint);
  unsigned int    i = 0;

  switch (strategy)
    {
      case CrossOverStrategy::OnePoint:
        while (i < crossPointBreak)
          {
            newAlgo->CopySequence(first, i);
            ++i;
          }
        while (i < AlgoGen::param->NbSequenceChromosome)
          {
            newAlgo->CopySequence(second, i);
            ++i;
          }
        break;

      case CrossOverStrategy::Uniform:
        while (i < AlgoGen::param->NbSequenceChromosome)
          {
            newAlgo->finger[i] = (i % 2 ? first.finger[i] : second.finger[i]);
            newAlgo->shoulder[i] = (i % 2 ? second.shoulder[i] : first.shoulder[i]);
            newAlgo->arm[i] = (i % 2 ? first.arm[i] : second.arm[i]);
            newAlgo->timer[i] = (i % 2 ? second.timer[i] : first.timer[i]);
            ++i;
          }
        break;

      default:
          abort(); // Ne devrait jamais pop.
        break;
    }
  return (newAlgo);
}

/*!
 * \brief Genere un nouveau chromosome, avec ses valeurs au hasard
 */
void    Chromosome::Mutation(Chromosome *individual)
{
  assert(individual != nullptr);
  Chromosome::Mutation(*individual);
}

/*!
 * \brief Fais muter un chromosome
 * \var PourcentageMutation
 * \brief Compris entre 0 et 100 (compris). Le chromosome sera (environ) PourcentageMutation different de son origine.
 */
void    Chromosome::Mutation(Chromosome &individual)
{
  int    PourcentageMutation = AlgoGen::param->PctMutationChromosome;
  int         PctOneMutation = static_cast<int>((1.f / (AlgoGen::param->NbSequenceChromosome * 4.f)) * 100.f);

  while (PourcentageMutation > 0)
    {
      unsigned int value = rand() % (AlgoGen::param->NbSequenceChromosome * 4);
      PourcentageMutation -= PctOneMutation; // Pourcentage pour UNE mutation

      switch (value % 4)
        {
          case 0:
            individual.finger.at(value / 4) = GenerateRandomValue(FINGER);
            break;

          case 1:
            individual.arm.at(value / 4) = GenerateRandomValue(ARM);
            break;

          case 2:
            individual.shoulder.at(value / 4) = GenerateRandomValue(SHOULDER);
            break;

          case 3:
            individual.timer.at(value / 4) = GenerateRandomValue(TIMER);
            break;
        }
    }
}

Chromosome    *Chromosome::Clone(const Chromosome *target)
{
  Chromosome    *ptr = new Chromosome();

  *ptr = *target;
  return (ptr);
}

Chromosome    *Chromosome::Clone(const Chromosome &target)
{
  return (Chromosome::Clone(&target));
}

std::string    Chromosome::ToString(StringType type) const
{
  std::stringstream    ss;

  assert(this != nullptr);
  switch (type)
    {
      case DECIMAL:
        for (unsigned int i = 0; i < AlgoGen::param->NbSequenceChromosome; i++)
          {
            ss.width(4);
            ss << this->finger[i] << "-";
            ss.width(4);
            ss << this->arm[i] << "-";
            ss.width(4);
            ss << this->shoulder[i] << "-";
            ss.width(4);
            ss << this->timer[i];
            if (i + 1 < AlgoGen::param->NbSequenceChromosome)
              ss << " | ";
          }
        break;

      case HEXA:
        for (unsigned int i = 0; i < AlgoGen::param->NbSequenceChromosome; i++)
          {
            std::string    str;

            Conversion::IntToBase(this->finger[i], "0123456789ABCDEF", str);
            ss.width(3);
            ss << str << "-";
            str.clear();

            Conversion::IntToBase(this->arm[i], "0123456789ABCDEF", str);
            ss.width(3);
            ss << str << "-";
            str.clear();

            Conversion::IntToBase(this->shoulder[i], "0123456789ABCDEF", str);
            ss.width(3);
            ss << str << "-";
            str.clear();

            Conversion::IntToBase(this->timer[i], "0123456789ABCDEF", str);
            ss.width(3);
            ss << str;
            if (i + 1 < AlgoGen::param->NbSequenceChromosome)
            ss << " | ";
            str.clear();
          }
        break;

      default:
        throw std::runtime_error("Chromosome::ToString : type invalide");
    }
  return (ss.str());
}

int                Chromosome::GetValue(Chromosome::PartType type, std::size_t pos) const
{
  switch (type)
    {
      case FINGER:
        return (finger.at(pos));

      case ARM:
        return (arm.at(pos));

      case SHOULDER:
        return (shoulder.at(pos));

      case TIMER:
        return (timer.at(pos));

      default:
        throw std::runtime_error("Chromosome::GetValue : type invalide");
    }
}

void            Chromosome::copyVector(std::vector<int> &vect, const std::vector<int> &target)
{
  vect.reserve(target.size());
  vect.clear();
  for (std::vector<int>::const_iterator cit = target.begin(); cit != target.end(); cit++)
    vect.push_back(*cit);
}

void            Chromosome::operator=(const Chromosome &other)
{
  copyVector(this->finger, other.finger);
  copyVector(this->arm, other.arm);
  copyVector(this->shoulder, other.shoulder);
  copyVector(this->timer, other.timer);
}

static bool intEqual(int val1, int val2)
{
  return (val1 == val2);
}

bool            Chromosome::operator==(const Chromosome &other) const
{
  if (std::equal(finger.begin(), finger.end(), other.finger.begin(), &intEqual))
    if (std::equal(arm.begin(), arm.end(), other.arm.begin(), &intEqual))
      if (std::equal(shoulder.begin(), shoulder.end(), other.shoulder.begin(), &intEqual))
        if (std::equal(timer.begin(), timer.end(), other.timer.begin(), &intEqual))
          return (true);
  return (false);
}

void            Chromosome::CopySequence(const Chromosome &origine, unsigned int Sequence)
{
  if (Sequence > AlgoGen::param->NbSequenceChromosome)
    {
      std::cerr << "Copy chromosomique invalide : " << Sequence << std::endl;
      throw std::runtime_error("Chromosome::CopySequence : Nombre invalide pour Sequence");
    }

  this->finger[Sequence] = origine.finger[Sequence];
  this->arm[Sequence] = origine.arm[Sequence];
  this->shoulder[Sequence] = origine.shoulder[Sequence];
  this->timer[Sequence] = origine.timer[Sequence];
}

int    Chromosome::GenerateRandomValue(Chromosome::PartType type)
{
  switch (type)
    {
      case FINGER:
        return (rand() % 1025);

      case ARM:
        return (rand() % 1025);

      case SHOULDER:
        return (rand() % 1025);

      case TIMER:
        return (rand() % 1400);

      default:
        throw std::runtime_error("Chromosome::GenerateRandomValue : type invalide");
    }
}
