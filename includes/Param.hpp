#pragma once

#include <cstddef>

enum CrossOverStrategy
{
  OnePoint, // Le premier jusqu'au crosspoint, le second apres.
  Uniform   // Un coup le premier, un coup le second, etc etc ...
};


/// \brief Conteneur de parametres.
struct AlgoParam
{
  /// \brief Utilise une population intermediaire pour choisir la generation suivante
  bool                UsePopIntermediare;

  /// \brief La taille de la population
  /// \attention Doit etre > 0 et ne pas etre trop petit.
  std::size_t         SizePopulation;

  /// \brief Le nombre de generation qui tourneront.
  /// \attention Doit etre > 1
  std::size_t         NbGenerationBeforeStop;

  /// \brief Le facteur d'acceleration de la simulation (a faire correspondre avec Marilou)
  /// \attention Doit etre > 0
  std::size_t         RealTimeMultiplier;

  /// \brief Le pourcentage d'individus qui seront completement aleatoire dans une generation.
  /// \attention Doit etre >= 0 || <= 100
  std::size_t         Pct_GenerationAleatoire;

  /// \brief Le pourcentage de chance qu'un individu random de la Fitness soit un cryo.
  /// \attention Doit etre >= 0 || <= 100
  std::size_t         Pct_Cryo;

  /// \brief Le nombre de chromosome dans un individu
  /// \attention Doit etre > 0
  std::size_t         NbSequenceChromosome;

  /// \brief Le nombre de fois que la suite de chromosome d'un individu sera repete.
  /// \attention Doit etre > 0
  std::size_t         NbRepetition;

  /// \brief Le taux de mutation d'un individu
  /// \attention Doit etre >= 0 && <= 100
  unsigned int        PctMutationChromosome;

  /// \brief La strategie de croisement
  CrossOverStrategy CrossOver_Strategy;
};

