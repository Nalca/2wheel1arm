#pragma once

namespace ApiBlli
{
  struct    Position
  {
    Position()
    {
      this->x = 0.0f;
      this->y = 0.0f;
      this->z = 0.0f;
    }

    Position(float _x, float _y, float _z)
    {
      this->x = _x;
      this->y = _y;
      this->z = _z;
    }

    Position(const Position &_position)
    {
      this->x = _position.x;
      this->y = _position.y;
      this->z = _position.z;
    }

    void equal(const Position *other)
    {
      this->x = other->x;
      this->y = other->y;
      this->z = other->z;
    }

    float    x;
    float    y;
    float    z;
  };
}