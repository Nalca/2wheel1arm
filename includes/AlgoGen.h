#pragma once

#include <iostream>
#include <vector>
#include <list>
#include <map>
#include "Individu.h"
#include "Exception.hpp"

// pour fonctionner avec le logger
#define LOG_LEVEL_EXE   4
#define LOG_LEVEL_BEST  5

namespace ApiBlli
{
  class Robot;
}
class IFitness;
struct AlgoParam;

NEW_EXCEPTION(AlgogenException, BasicException);

class AlgoGen
{
private: // Non implemente
  AlgoGen();

public:
  typedef std::list<Individu *> Tpop;
  AlgoGen(const AlgoParam *param, ApiBlli::Robot *_robot);
  ~AlgoGen();


  void    Run();

  void    TestConfig();

  /// \throw FitnessException si la Fitness est deja enregistree où n'existe pas.
  void    FitnessAdd(const std::string &name, std::size_t FitPop, std::size_t FitCryoPop);

  /// \throw FitnessException si la Fitness n'est pas enregistree.
  void    FitnessRemove(const std::string &name);

  /// \return Un pointeur sur la fitness si elle existe, nullptr sinon
  const IFitness *FitnessIsUsed(const std::string &name) const;

  /// \brief Evalue un individu <i>nbEval</i> fois, et retourne le resultat dans <i>result</i>.
  void  EvaluteIndividu(const Individu *testIndividual, std::list<Individu *> &result, std::size_t nbEval);

  /// \brief Evalue un individu. Si nbRepetition = 0, la valeur dans param sera prise.
  void  EvaluteIndividu(Individu *testIndividual, std::size_t nbRepetition = 0);
  
  void    GenerateRandomPopulation(Tpop &emptyPopulation);

  /// \brief Teste une population avec le simulateur
  void    EvaluatePopulation(Tpop &testPopulation);

  /// \brief Creer une nouvelle population a partir d'une seule population.
  void    SelectionAndReproduction(Tpop &popTarget);

  void    GotoNextGeneration(); // ++currentGeneration && switch pop
  void    FitnessShowBest() const;

  // Log

// Informations
  void                    ResetGenerationCounter();
  std::size_t             GetCurrentGeneration() const;
  static const AlgoParam  *param;

protected:
  void  DeletePopulation(Tpop &popToKill);

public: // Ne pas toucher manuellement
  std::list<Individu *>   pop_Parent;
  std::list<Individu *>   pop_Child;
  std::list<Individu *>   pop_NextGen;
  std::size_t             currentGeneration;

private:
  std::map<std::string, IFitness *> FitnessMap;
  ApiBlli::Robot *robot;
};
