#pragma once

#include <string>
#include <vector>
#include "Param.hpp"

class Chromosome
{
public:
  enum StringType
  {
    DECIMAL,
    HEXA
  };

  enum PartType
  {
    FINGER,
    ARM,
    SHOULDER,
    TIMER
  };

public:
  Chromosome();
  Chromosome(std::size_t sizeVector);
  ~Chromosome();

  static Chromosome *GetRandom();
  static Chromosome *CrossOver(const Chromosome *first, const Chromosome *second, float CrossPoint = 0.5, CrossOverStrategy strategy = OnePoint);
  static Chromosome *CrossOver(const Chromosome &first, const Chromosome &second, float CrossPoint = 0.5, CrossOverStrategy strategy = OnePoint);
  static void       Mutation(Chromosome *individual);
  static void       Mutation(Chromosome &individual);
  static Chromosome *Clone(const Chromosome *target);
  static Chromosome *Clone(const Chromosome &target);

  std::string       ToString(Chromosome::StringType type = HEXA) const;
  int               GetValue(Chromosome::PartType type, std::size_t pos) const;
  void              operator=(const Chromosome &other);
  bool              operator==(const Chromosome &other) const;

private:
  void              CopySequence(const Chromosome &origine, unsigned int Sequence);
  static int        GenerateRandomValue(Chromosome::PartType type);
  void              copyVector(std::vector<int> &vect, const std::vector<int> &target);

public:
  std::vector<int>  finger;
  std::vector<int>  arm;
  std::vector<int>  shoulder;
  std::vector<int>  timer;
};
