#pragma once

#include <ModaCPP.h>
#include <map>
#include <list>
#include "ApiBlliPosition.hpp"

namespace ApiBlli
{
  class Leg
  {
  public:
    Leg(ModaCPP::RobotPHX *pPHX, const xkode::lib::String &name, bool left);
    ~Leg(void);
    void    SetAngle(int servo, float angle, float speed);
    float    GetAngle(int servo);
    void    SetIndex(int servo, int index, float speed);
    int        GetIndex(int servo);
    Position    *GetPosition(int servo);
    Position    *GetFingerPosition();

    float    GetTorque(int servo);
    const xkode::lib::String    &GetName();
    bool    IsInColision();
    bool    IsTuchingGround();

    enum
    {
      FINGER = 0,
      ARM,
      SHOULDER
    };

  private:
    ModaCPP::RobotPHX                            *_pPHX;
    std::map<int, ModaCPP::DeviceServoMotor *>    _Servos;
    std::map<int, ModaCPP::DeviceForceSensor *>    _Sensors;
    std::list<ModaCPP::DeviceContact *>            _ContactBox;
    std::map<int, ModaCPP::Geom *>                _ContactBoxServo;

    ModaCPP::DeviceContact                        *_GroundBoxContact;
    ModaCPP::Geom                                *_FingerBox;

    xkode::lib::String              _Name;
    bool                          _Left;
  };
}
