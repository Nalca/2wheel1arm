#pragma once
#include <list>
#include <string>
#include <fstream>
#include "tools/includes/Shell.hpp"

namespace ApiBlli {  class Robot;}
class AlgoGen;
struct AlgoParam;

void  add_fitness(void *data, const std::list<std::string> &listArg);
void  remove_fitness(void *data, const std::list<std::string> &listArg);
void  list_fitness(void *data, const std::list<std::string> &listArg);
void  start(void *data, const std::list<std::string> &listArg);
void  use_default_conf(void *data, const std::list<std::string> &listArg);
void  DNAfromfile(void *data, const std::list<std::string> &listArg);

/// \brief De la forme "runseq [fitnessName] [nb Iteration]". Montre les codes des individus, leurs resultats, et permet d'en jouer sur le simu.
void  runseq(void *data, const std::list<std::string> &listArg);

class Commander : public Shell
{
public:
  Commander(const char *prompt = "");
  virtual ~Commander();

  void            FillParam();
  void            initOneTimeRobot();
  void            initOneTimeAlgogen();

  /// \brief Ajoute les fonctions specifiques du projet au shell, fait apres les autres init a cause d'un probleme d'ordre d'initialisation.
  void            addFunc();
  AlgoGen         *algogen;

protected:
  ApiBlli::Robot  *getRobot();

private:
  ApiBlli::Robot  *robot;
  AlgoParam       *param;
  bool            logIsOpened;
};