#pragma once

#include <iostream>
#include <vector>
#include "Chromosome.h"
#include "ApiBlliPosition.hpp"

#define PI 3.1415926f
#define TO_RAD(value) (value * PI / 180)
#define TO_DEG(value) (value * 180 / PI)

class Individu
{
public:
  ~Individu();

  static Individu *GetNewInstance(Chromosome *newChromosome = nullptr);
  static Individu *Clone(const Individu *target, bool withPositions = false);
  static Individu *Clone(const Individu &target, bool withPositions = false);
  static bool     CompareChromosome(const Individu *ind1, const Individu *ind2);

  void            SetChromosome(Chromosome *newAlgo);

    /// \brief Retourne la distance parcourue
  const ApiBlli::Position                 &GetDistanceTotale() const;
  void                                    AddPosition(ApiBlli::Position *position);
  const std::vector<ApiBlli::Position *>  &GetPositions() const;

  const Chromosome        &GetChromosome() const;
  Chromosome              &GetChromosome();

  void                    ShowChromosomeDiff(const Individu *other, std::ostream &os = std::cout, Chromosome::StringType type = Chromosome::HEXA) const;

  void    log(std::ostream &os);
  bool    operator==(const Individu &other) const;
  
private:
  Individu();
  Individu(const Individu &other);
  void    copyPositions(const Individu *target);

  std::vector<ApiBlli::Position *>  positions;
  ApiBlli::Position                 *dist;
  Chromosome                        *algo;
};
