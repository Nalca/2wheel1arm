#pragma once

#include <cstddef>
#include <string>
#include <list>
#include "Exception.hpp"

NEW_EXCEPTION(FitnessException, BasicException);

class AlgoGen;
class Individu;
class IFitness
{
public:
  /// \enum TypePop Une enum utilisee pour designer le type de population dans une fitness.
  enum TypePop
  {
    GENERATION,
    CRYO
  };

  typedef bool (*FuncCmp)(const Individu *, const Individu *);
  
  IFitness(const std::string &_name, std::size_t _FitPop, std::size_t _FitCryoPop);
  virtual ~IFitness();

  /// \brief Evalue un individu
  virtual void  EvaluateIndividual(const Individu *ind) = 0;

  /// \brief Laisse la fitness faire ses tests / cryogenisation, avant de generer une nouvelle generation
  virtual void  EvaluateGeneration(AlgoGen *algo) = 0;

  /// \brief Supprimer le conteneur de population, pour faire de la place pour la generation suivante.
  virtual void  DeleteGenerationResult();


  bool            CanReturnIndividual() const;
  bool            CanReturnIndividual(TypePop pop) const;
  
  /*!
   * \brief Renvoie un (clone) individu au hasard parmi la population de la Fitness
   * \var PctBest
   * \brief Le pourcentage de change d'avoir un individu cryo, sur 100.
   * \attention Le cas ou il n'y a aucun individu cryogenise (actuellement) existe !
   */
  virtual Individu  *GetCloneRandomIndividual(std::size_t PctBest) const;

  /*!
   * \brief Renvoie une reference sur l'individu au hasard parmi la population de la Fitness
   * \var PctBest
   * \brief Le pourcentage de change d'avoir un individu cryo, sur 100.
   * \attention Le cas ou il n'y a aucun individu cryogenise (actuellement) existe !
   */
  virtual const Individu *GetRandomIndividual(std::size_t PctBest) const;

  /*!
   * \brief Renvoie l' (clone) individu qui repond le mieux a la Fitness. Commence a 0.
   * \attention Si num > population, renvoie un % (modulo)
   * \attention Renvoie le meilleur de la popGen si aucun n'est present dans la cryo
   */
  virtual Individu  *GetCloneBestIndividual(TypePop pop, std::size_t num = 0) const;

  /*!
   * \brief Renvoie une reference constante sur le meilleur individu contenu dans la fitness
   * \attention Si num > population, renvoie un % (modulo)
   * \attention Renvoie le meilleur de la popGen si aucun n'est present dans la cryo
   */
  virtual const Individu *GetBestIndividual(TypePop pop, std::size_t num = 0) const;

  /// \brief Retourne un pointeur sur une Fitness, selon son nom
  /// \return La fitness si elle existe,  nullptr sinon
  static IFitness  *GetFitness(const std::string &name, std::size_t FitPop, std::size_t FitCryoPop);

  const std::list<Individu *> &GetGenPop() const
  {
    return (genPop);
  }

  const std::list<Individu *> &GetCryoPop() const
  {
    return (cryoPop);
  }
  
protected:
  const std::size_t FitPop;
  const std::size_t FitCryoPop;
  const std::string name;

  std::list<Individu *> genPop;
  std::list<Individu *> cryoPop;

  /// \brief Insère un individu dans une liste, en supprimant celui au fond (back)
  /// \attention L'individu est delete si il ne peut pas être inséré
  void            InsertIndividuAtBack(TypePop pop, Individu *ind);

  void            DeletePopulation(TypePop pop);
  void            SortPopulation(TypePop pop, FuncCmp);

private:
  const Individu *GetInternalRandom(TypePop pop) const;
  
public:
    /// \brief Retourne le nom de la fonction de fitness
  const std::string &getName() const
  { return (name); }

  /// \brief Retourne la taille de la population
  std::size_t getFitnessPopulation() const
  { return (FitPop); }

  /// \brief Retourne la taille de la population congelee.
  std::size_t getFitnessCryoPopulation() const
  { return (FitCryoPop); }
};