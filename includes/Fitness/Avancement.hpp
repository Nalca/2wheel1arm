#pragma once

#include <list>
#include "Fitness/Fitness.hpp"

namespace Fitness
{
    /// \todo V�rifier qu'il avance � chaque fois.
    /// \todo V�rifier qu'il est sur ses deux roues
  class Avancement : public IFitness
  {
  public:
    Avancement(const std::string &_name, std::size_t _FitPop, std::size_t _FitCryoPop);
    ~Avancement();

    static bool compSupDistance(const Individu *ind1, const Individu *ind2);
    static bool lowerBound(const Individu *ind, float meanX);
    
    /// \attention Si la population est plus petite que la taille de population de la fitness, tout les individus (memes les manchots) seront des reproducteurs
    void  EvaluateIndividual(const Individu *ind);

    /// \brief Passe des membres dans cryoPop avant l'extermination de genPop
    void  EvaluateGeneration(AlgoGen *algo);

  private:
    /// \brief Pour savoir si l'individu est "ok" du point de vu de la fitness
    bool  TestIndividual(const Individu *ind);
  };
}