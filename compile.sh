#!/bin/bash
# Author : Alexandre Frizac
# Creation time : 12/02/2013
## Last update Tue Apr 16 16:45:44 2013 Alexandre Frizac

### Variable de configuration
# Répertoire utilisé pour la compilation
BUILD_DIRECTORY="build"
# Nom de l'executable
EXECUTABLE_NAME="2Whell1Arm"
# Lance le programme avec ces options
EXECUTABLE_OPTIONS="script"


### Specifique à valgrind
# Options utilisées avec valgrind
VALGRIND_OPTIONS="--leak-check=full"
# Fichier de log pour valgrind
VALGRIND_LOG="vallog.log"

### Initialisation des variables
EXECUTABLE_PATH="${BUILD_DIRECTORY}/${EXECUTABLE_NAME}"
BUILD="YES"                    ## Compile (si besoin)
DEBUG="NO"                     ## Ajoute les flags de debug
CLEAN="NO"                     ## Clean $BUILD_DIRECTORY
USE_VALGRIND="NO"              ## Lance l'executable avec valgrind     (exclu gdb)
USE_GDB="NO"                   ## Lance l'executable avec gdb          (exclu valgrind)
CPU_COUNT=`nproc`              ## Compte le nombre de cpu du pc (pour make -j)
RUN_EXECUTABLE="NO"            ## Lance le programme compilé après la compilation
MAKE_DOC="NO"                  ## Compile la doc avec doxygen

exitErr()
{
    echo "Error while building, stopping \`$0\` now."
    exit -1
}

if [ "$#" -gt "0" ]; then
    for arg in $@; do
    case "${arg}" in
        "pp")
        echo "sources/ & includes/ files :"
	echo ""
        find sources/ -name "*.cpp" | xargs ls --width=1 | sed 's/^/  /g'
        echo ""
	find includes/ -name "*.hpp" -or -name "*.hh" -or -name "*.h" | xargs ls --width=1 | sed 's/^/  /g'
	echo ""

	echo ""
	find tools/sources/ -name "*.cpp" | xargs ls --width=1 | sed 's/^/  /g'
        echo ""
        find tools/includes/ -name "*.hpp" -or -name "*.hh" -or -name "*.h" | xargs ls --width=1 | sed 's/^/  /g'
	echo ""

	BUILD="NO"
        ;;

        "re")
        CLEAN="YES"
        BUILD="YES"
        ;;

        "clean")
        CLEAN="YES"
        BUILD="NO"
        ;;

        "build")
        BUILD="YES"
        DEBUG="YES"
        ;;

        "debug")
        BUILD="YES"
        DEBUG="YES"
        ;;

        "run")
        RUN_EXECUTABLE="YES"
        ;;

        "gdb")
        BUILD="YES"
        DEBUG="YES"
        RUN_EXECUTABLE="YES"
        UES_VALGRIND="NO"
        USE_GDB="YES"
        ;;

        "valgrind")
        BUILD="YES"
        DEBUG="YES"
        RUN_EXECUTABLE="YES"
        USE_VALGRIND="YES"
        USE_GDB="NO"
        ;;

        "doc")
        MAKE_DOC="YES"
        CLEAN="YES"
        BUILD="NO"
        ;;

        *)
        if [ "${arg}" != "--help" ]; then
            echo "Unknown argument : ${arg}"
        fi
        echo ""
                echo "Options possible : "
        echo "pp      : list all *.hpp, *.cpp, *.h, *.hh files"
                echo "re       : remove ${BUILD_DIRECTORY} then compile ${EXECUTABLE_NAME}"
                echo "clean    : remove ${BUILD_DIRECTORY}"
        echo "build    : compile ${EXECUTABLE_NAME}, use after clean"
        echo "debug    : compile with -ggdb3 ; you MUST clean if you want to build without debugging information"
        echo "run      : run ${EXECUTABLE_NAME} after compiling it IF ${EXECUTABLE_NAME} exist"
        echo "gdb      : compile with debug flag, then run ${EXECUTABLE_NAME} with gdb"
        echo "valgrind : compile with debug flag, then run ${EXECUTABLE_NAME} with valgrind"
        echo "doc      : compile documentation with doxygen, mais supprime le dossier build avant"
        exit
        ;;
    esac
    done
fi

if [ "${CLEAN}" = "YES" ]; then
    echo "rm -rf ${BUILD_DIRECTORY}"
    rm -rf ${BUILD_DIRECTORY}
fi

if [ "${MAKE_DOC}" = "YES" ]; then
    doxygen Doxyfile && firefox doc/html/index.html &
fi

if [ "${BUILD}" = "YES" ]; then
    mkdir -p ${BUILD_DIRECTORY} && cd ${BUILD_DIRECTORY} || (echo "Error while creating ${BUILD_DIRECTORY}" && exit)
    if [ "${DEBUG}" = "YES" ]; then
    cmake -DDEBUG_FLAGS=YES .. || exitErr
    else
    cmake .. || exitErr
    fi

    echo ""
    echo "Using ${CPU_COUNT} cpu(s) for compilation:"
    make -k -j ${CPU_COUNT} || exitErr
    cd ..
fi

if [ "${RUN_EXECUTABLE}" = "YES" ]; then
    echo ""
    echo "Running \`${EXECUTABLE_PATH}\` ..."

    if [ -a "${EXECUTABLE_PATH}" ]; then
    if [ "${USE_VALGRIND}" = "YES" ]; then
        echo "Note : Si valgrind plante, le script peut s'arrêter ici ..."
        if [ "${VALGRIND_LOG}" == "" ]; then
	    VALGRIND_COMMANDLINE="valgrind ${VALGRIND_OPTIONS} ${EXECUTABLE_PATH} ${EXECUTABLE_OPTIONS}"
	    echo ${VALGRIND_COMMANDLINE}
	    ${VALGRIND_COMMANDLINE}
        else
	    VALGRIND_COMMANDLINE="valgrind ${VALGRIND_OPTIONS} ${EXECUTABLE_PATH} ${EXECUTABLE_OPTIONS}"
	    echo "${VALGRIND_COMMANDLINE} 2>&1 | tee ${VALGRIND_LOG}"
	    ${VALGRIND_COMMANDLINE} 2>&1 | tee ${VALGRIND_LOG}
        fi
    elif [ "${USE_GDB}" = "YES" ]; then
        gdb --args ${EXECUTABLE_PATH} ${EXECUTABLE_OPTIONS}
    else
        "${EXECUTABLE_PATH}" ${EXECUTABLE_OPTIONS}
    fi
    else
    echo "Le fichier ${EXECUTABLE_PATH} n'existe pas."
    fi
fi

